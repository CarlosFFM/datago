USE CEIMdC;

INSERT INTO CargoSueldo(Cargo, Sueldo)
VALUES("Profesor P", 500),
("Asistente", 450),
("Adminis", 700),
("Rectora", 700),
("Conserje", 700);

INSERT INTO Horario(tipo, horaEnt, horaSal)
values ("matutino",'07:00:00','15:00:00'),
("otro mat",'07:00:00','15:00:00'),
("conserje",'07:00:00','17:00:00'),
("ayudantes",'08:00:00','13:00:00'),
("directivo",'07:00:00','14:30:00');

INSERT INTO Empleado 
VALUES('0923432166','Juan Marcos','Olivos Masa',1,2),
('0987542177','Felipe Luis','Aguirre Gonzaga',5,1),
('0706412773','Carlos Mateo','Noboa Satoalla',5,1),
('0964783621','Viviana Miriam','Ramirez Rodriguez',1,1),
('0908878638','Fernado Juan','Jara Sinchi',2,1),
('0903971146','Diego Joel','Rojas Loayza',2,1);

INSERT INTO Multa(valor,Descripcion,Fecha,Hora,Empleado)
values(100,'imputual','2019-01-10','07:22:00','0923432166'),
(100,'imputual','2019-01-09','07:15:00','0987542177'),
(100,'imputual','2019-01-09','07:01:00','0706412773'),
(100,'impuntual','2019-01-11','07:02:00','0964783621'),
(100, 'impuntual','2019-01-08','07:03:00','0908878638');

INSERT INTO Profesor 
VALUES('0923432166','Superior', null),
('0987542177','Superior', null),
('0706412773','Superior', null),
('0964783621','Superior', null),
('0908878638','Superior', null);

INSERT INTO Representante
VALUES ('1202075618','Luisa	Rosario','Jimenez','1984-09-23','Ecuador','0938475934','Superior',1,'Casado',0,'luirosa@hotmail.com','Secretaria'),
('0398573819','	Marcelo Tato','Cruz','1987-02-11','Ecuador','0938127458','Superior',1,'Soltero',1,'titotato@gmail.com','Ing Civil'),
('1284994012','	Marcos Tomás','Duran','1985-05-21','Ecuador','0938495819','Superior',0,'Soltero',0,'marto12@live.com','Chef'),
('0938475812','	María Victor','Carrasco','1970-10-14','Ecuador','0938457829','Superior',1,'Casado',1,'carracd@hotmail.com','Ing Mecanico'),
('0948491235','Juan Luis','Guerra','1975-08-28','Ecuador','0923859103','Superior',1,'Casado',0,'guerrA34@hotmail.com','Doctor');

INSERT INTO Estudiante
VALUES ('1205801515','Karla Antonieta','Duran Oscuez','2005-08-28','042212831','ecuatoriana','Mamá','Samanes','1202075618','1202075618','1202075618',0),
('0938291023','Jaime Gabriel','Gutierrez Santos','2003-12-01','042212123','ecuatoriana','Papá','Sauces 8','0398573819','0398573819','0398573819',0),
('1284920394','Beto Santiago','Heilsnerr','2006-02-09','042215677','aleman','Mamá','Matices','1284994012','1284994012','1284994012',1),
('1205381514','Maria Belen','Gustaff Oscuez','2006-03-14','042241785','colombiano','Papá','Guayacanes','0938475812','0938475812','0938475812',1),
('0182847013','Sarita Lisa','Roma Jaramillo','2005-10-15','123049585','ecuatoriana','Papá','Mapasingue','0948491235','0948491235','0948491235',1);

INSERT INTO Representados
VALUES ('1202075618', '1205801515', 'Otro'),
('0398573819', '0938291023', 'Otro'),
('1284994012', '1284920394', 'Otro'),
('0938475812', '1205381514', 'Otro'),
('0948491235', '0182847013', 'Otro');
