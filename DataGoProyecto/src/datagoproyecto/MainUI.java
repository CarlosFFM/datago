package datagoproyecto;

import datagoproyecto.vistas.Login;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author CarlosFFM
 */
public class MainUI extends Application {
    
    private Parent root;
    Stage primaryStage;
    
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        root = new Login(this);
        Scene scene = new Scene(root, 1280, 720);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public void changeUI(Parent otro){
        primaryStage.close();
        root = otro;
        Scene oscene = new Scene(root, 1280, 720);
        primaryStage.setScene(oscene);
        primaryStage.show();
    }
}
