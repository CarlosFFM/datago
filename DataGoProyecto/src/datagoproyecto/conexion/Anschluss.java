package datagoproyecto.conexion;

/*
Se dbe tener el JAR del Conector/J en el JRE o JDK, lo que use el IDE
 */
import datagoproyecto.vistas.displayMultas;
import datagoproyecto.vistas.displayAlumnos;
import datagoproyecto.vistas.displayPagos;
import datagoproyecto.vistas.displayProfs;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author DataGo
 */
public class Anschluss {

    private final String username;
    private final String password;
    private String serverIP;
    private Connection cdb;

    public Anschluss(String username, String password) {
        this.username = username;
        this.password = password;
        this.serverIP = "127.0.0.1"; //Cambiarla con la IP de la maquina que tenga la base
        try {
            cdb = DriverManager
                    .getConnection("jdbc:mysql://" + serverIP + ":3306/CEIMdC",
                            username, password);
            System.out.println("Se creo connector");
        } catch (SQLException e) {
            cdb = null;
            System.out.println(e);
        }
    }

    public ResultSet runQuery(String q) {
        try {
            ResultSet ret = probarConexion(cdb, q);
            mostrarResultados(ret);
            return ret;
        } catch (SQLException ex) {
            return null;
        }
    }

    /* TODO:
    *Hay que usar esta clase para realizar los queries, aquí se
    *se escapan los queries y se mandan a la DB, luego se pasan a la 
    *vista correspondiente.
     */
    private static String getQuery() {
        String q;
        try (Scanner s = new Scanner(System.in)) {
            q = s.nextLine();
        }
        return q;
    }

    /*
    Mucho más organizado hacerlo de esta manera, ayuda a la mantenibilidad.
     */
    private static ResultSet probarConexion(Connection c, String q)
            throws SQLException {
        Statement s = c.createStatement();
        ResultSet ret = s.executeQuery(q);
        return ret;
    }

    /*
    Metodo para imprimir cualquier resultado, pero poco más.
     */
    private static void mostrarResultados(ResultSet s) throws SQLException {
        ResultSetMetaData dMeta = s.getMetaData();
        int cols = dMeta.getColumnCount();

        //Mostrar los resultados
        for (int i = 1; i <= cols; i++) {
            if (i > 1) {
                System.out.print(", ");
            }
            System.out.print(dMeta.getColumnName(i));
        }
        while (s.next()) {
            System.out.println("");
            for (int i = 1; i <= cols; i++) {
                if (i > 1) {
                    System.out.print(", ");
                }
                String val = s.getString(i);
                System.out.print(val);
            }
        }
        System.out.println("");
    }

    public boolean isConnected() {
        return cdb != null;
    }

    public void close() {
        if (cdb != null) {
            try {
                cdb.close();
            } catch (SQLException ex) {
                System.out.println(ex);
            }
        }
    }

    public boolean matricular(String nomAlum,
            String apeAlum,
            String cedAlum,
            LocalDate fdnAlum,
            String nacAlum,
            String telCasAlum,
            String conEmerAlum,
            String dirAlum,
            String nomPad,
            String apePad,
            String cedPad,
            LocalDate fdnPad,
            String nacPad,
            String celPad,
            String nivelPad,
            boolean autPad,
            String ecPad,
            boolean vcePad,
            String emailPad,
            String ocupPad,
            String nomMad,
            String apeMad,
            String cedMad,
            LocalDate fdnMad,
            String nacMad,
            String celMad,
            String nivelMad,
            boolean autMad,
            String ecMad,
            boolean vceMad,
            String emailMad,
            String ocupMad,
            String nomRep,
            String apeRep,
            String cedRep,
            LocalDate fdnRep,
            String nacRep,
            String celRep,
            String nivelRep,
            boolean autRep,
            String ecRep,
            boolean vceRep,
            String emailRep,
            String ocupRep) {

        try {
            java.sql.Date fdnAlumDate = java.sql.Date.valueOf(fdnAlum);
            PreparedStatement ingAlum = cdb.prepareStatement("INSERT INTO "
                    + "Estudiante(Cedula, Nombre, Apellido, FechaNacimiento, "
                    + "TelefonoCasa, ContactoEmer, Direccion, Madre, Padre, Representante) "
                    + "VALUES (?,?,?,?,?,?,?,?,?,?)");
            boolean PadEsRep = cedPad.equals(cedRep);
            boolean MadEsRep = cedMad.equals(cedRep);

            try {
                insRep(cedPad, nomPad, apePad, fdnPad, nacPad, celPad, nivelPad,
                        autPad, ecPad, vcePad, emailPad, ocupPad);
                insRep(cedMad, nomMad, apeMad, fdnMad, nacMad, celMad, nivelMad,
                        autMad, ecMad, vceMad, emailMad, ocupMad);
                if (PadEsRep) {
                    insRelRep(cedPad, cedAlum, "Padre");
                } else if (MadEsRep) {
                    insRelRep(cedMad, cedAlum, "Madre");
                } else {
                    insRep(cedRep, nomRep, apeRep, fdnRep, nacRep, celRep,
                            nivelRep, autRep, ecRep, vceRep, emailRep, ocupRep);
                    insRelRep(cedRep, cedAlum, "Otro");
                }
            } catch (Exception e) {
                System.out.println(e);
            }

            ingAlum.setString(1, cedAlum);
            ingAlum.setString(2, nomAlum);
            ingAlum.setString(3, apeAlum);
            ingAlum.setDate(4, fdnAlumDate);
            ingAlum.setString(5, telCasAlum);
            ingAlum.setString(6, conEmerAlum);
            ingAlum.setString(7, dirAlum);
            ingAlum.setString(8, cedMad);
            ingAlum.setString(9, cedPad);
            ingAlum.setString(10, cedRep);

            int insAlum = ingAlum.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }

    private boolean insRep(String ced, String nom, String ape, LocalDate fdn,
            String nac, String cel, String nivel, boolean aut, String ec,
            boolean vce, String email, String ocup) {
        try {
            java.sql.Date fdnDate = java.sql.Date.valueOf(fdn);
            PreparedStatement ingRep = cdb.prepareStatement("INSERT INTO "
                    + "Representante VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
            ingRep.setString(1, ced);
            ingRep.setString(2, nom);
            ingRep.setString(3, ape);
            ingRep.setString(5, nac);
            ingRep.setString(6, cel);
            ingRep.setString(7, nivel);
            ingRep.setString(9, ec);
            ingRep.setString(11, email);
            ingRep.setString(12, ocup);
            ingRep.setDate(4, fdnDate);
            ingRep.setBoolean(8, aut);
            ingRep.setBoolean(10, vce);

            int suc = ingRep.executeUpdate();

            return suc != 0;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    private boolean insRelRep(String cedR, String cedA, String Rel) {
        try {
            PreparedStatement ingRelRep = cdb.prepareStatement("INSERT INTO "
                    + "Representados VALUES (?,?,?)");
            ingRelRep.setString(1, cedR);
            ingRelRep.setString(2, cedA);
            ingRelRep.setString(3, Rel);

            int suc = ingRelRep.executeUpdate();
            return suc != 0;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    public ArrayList<displayPagos> getPagos(String ced, String fecha) {
        ArrayList<displayPagos> ret = new ArrayList<>();
        try {
            /*Nombre, Apellido, MesAnio, Valor, Estado, Descripcion*/
            PreparedStatement cvPagos
                    = cdb.prepareStatement("CALL getPagos(?,?)");

            cvPagos.setString(1, ced);
            cvPagos.setString(2, fecha);

            ResultSet pagos = cvPagos.executeQuery();

            ResultSetMetaData dMeta = pagos.getMetaData();
            int cols = dMeta.getColumnCount();

            /*Mostrar los resultados
            for(int i = 1; i <= cols; i++){
                    if (i > 1) System.out.print(", ");
                    System.out.print(dMeta.getColumnName(i));
                }*/
            while (pagos.next()) {
                ArrayList<String> vals = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    vals.add(pagos.getString(i));
                }
                ret.add(new displayPagos(vals.get(0), vals.get(1), vals.get(2),
                        vals.get(3), vals.get(4), vals.get(5), this));
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return ret;
    }

    public void marcarPagado(String nom, String ape, String fecha, int intentos) {
        try {
            PreparedStatement mP
                    = cdb.prepareStatement("CALL marcarPagado(?,?,?)");

            mP.setString(1, nom);
            mP.setString(2, ape);
            mP.setString(3, fecha);

            int suc = mP.executeUpdate();

            if (suc == 0 && intentos < 3) {
                marcarPagado(nom, ape, fecha, intentos++);
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public ArrayList<displayAlumnos> buscarAlumno(String nombre, String apellido, String cedula) {
        ArrayList<displayAlumnos> ret = new ArrayList<>();
        try {
            PreparedStatement consulta = cdb.prepareStatement("CALL consultaAlumno(?,?,?);");
            consulta.setString(1, nombre);
            consulta.setString(2, apellido);
            consulta.setString(3, cedula);

            ResultSet rs = consulta.executeQuery();

            ResultSetMetaData dMeta = rs.getMetaData();
            int cols = dMeta.getColumnCount();

            while (rs.next()) {
                ArrayList<String> l = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    String val = rs.getString(i);
                    l.add(val);
                }
                ret.add(new displayAlumnos(l.get(0), l.get(1), l.get(2), l.get(3),
                        l.get(4), l.get(5), l.get(6), l.get(7), l.get(8), l.get(9),
                        l.get(10), l.get(11)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return ret;
    }

    public ArrayList<displayMultas> getMultas(String nom, String ape, String fecha) {
        ArrayList<displayMultas> str = new ArrayList<>();
        try {
            PreparedStatement cvMultas = cdb.prepareStatement("CALL consultaMulta(?,?,?)");
            cvMultas.setString(1, nom);
            cvMultas.setString(2, ape);
            cvMultas.setString(3, fecha);

            ResultSet multas = cvMultas.executeQuery();
            ResultSetMetaData meta = multas.getMetaData();

            int cols = meta.getColumnCount();
            while (multas.next()) {
                ArrayList<String> vals = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    vals.add(multas.getString(i));
                }
                str.add(new displayMultas(vals.get(0), vals.get(1), vals.get(2),
                        vals.get(3), vals.get(4), vals.get(5), vals.get(6), this));

            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return str;
    }

    public ArrayList<displayPagos> getInpagos(String mes) {
        ArrayList<displayPagos> ret = new ArrayList<>();
        try {
            /*Nombre, Apellido, MesAnio, Valor, Estado, Descripcion*/
            PreparedStatement die = cdb.prepareStatement("DROP VIEW IF EXISTS Inpagos");
            
            int sucdel = die.executeUpdate();
            
            PreparedStatement cvPagos
                    = cdb.prepareStatement("CREATE VIEW Inpagos AS (SELECT Nombre, Apellido, MesAnio, Valor, Estado, Descripcion FROM (SELECT Cedula, Nombre, Apellido FROM Estudiante) e JOIN (SELECT Estudiante, MesAnio, Valor, Estado, Descripcion FROM Pagos WHERE MesAnio LIKE ? AND Estado = 'SIN PAGAR') p ON p.Estudiante = e.Cedula)");

            
            if (mes.length() == 1) mes = "0" + mes;
            
            cvPagos.setString(1,mes + "-____");

            int suc = cvPagos.executeUpdate();
            
            PreparedStatement vPagos = cdb.prepareStatement("SELECT * FROM Inpagos");
            
            ResultSet pagos = vPagos.executeQuery();

            ResultSetMetaData dMeta = pagos.getMetaData();
            int cols = dMeta.getColumnCount();

            while (pagos.next()) {
                ArrayList<String> vals = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    vals.add(pagos.getString(i));
                }
                ret.add(new displayPagos(vals.get(0), vals.get(1), vals.get(2),
                        vals.get(3), vals.get(4), vals.get(5), this));
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return ret;
    }
    
    public ArrayList<displayProfs> getProfs(String ced) {
        ArrayList<displayProfs> ret = new ArrayList<>();
        try {
            /*Nombre, Apellido, MesAnio, Valor, Estado, Descripcion*/
            PreparedStatement die = cdb.prepareStatement("DROP VIEW IF EXISTS Profs");
            
            int sucdel = die.executeUpdate();
            
            PreparedStatement cvProfs
                    = cdb.prepareStatement("CREATE VIEW Profs AS (SELECT Nombre, Apellido, p.Capacitaciones, i.materia, s.nivel, COUNT(m.id) AS Multas FROM (SELECT * FROM Empleado WHERE Cedula = ?) e JOIN Profesor p on e.Cedula = p.Cedula JOIN Imparte i on p.Cedula = i.Profesor JOIN Salon s ON i.Salon = s.ID JOIN Multa m on m.Empleado = e.Cedula)");
            
            cvProfs.setString(1,ced);

            int suc = cvProfs.executeUpdate();
            
            PreparedStatement vProfs = cdb.prepareStatement("SELECT * FROM Profs");
            
            ResultSet pagos = vProfs.executeQuery();

            ResultSetMetaData dMeta = pagos.getMetaData();
            int cols = dMeta.getColumnCount();

            while (pagos.next()) {
                ArrayList<String> vals = new ArrayList<>();
                for (int i = 1; i <= cols; i++) {
                    vals.add(pagos.getString(i));
                }
                ret.add(new displayProfs(vals.get(0), vals.get(1), vals.get(2),
                        vals.get(3), vals.get(4), vals.get(5)));
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return ret;
    }
}
