package datagoproyecto.vistas;

import datagoproyecto.conexion.Anschluss;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class VistaMultas extends AnchorPane {
    
    private Anschluss cdb;
    protected final VBox vBox;
    protected final Label lbl_Multas;
    protected final HBox txt_Nombres;
    protected final Label lb_Nombres;
    protected final TextField tf_Nombres;
    protected final Label label1;
    protected final TextField tf_Apelllidos;
    protected final Label lbl_Fecha;
    protected final DatePicker dt_fecha;
    protected final Button btn_Buscar;
    protected final ScrollPane spMultas;
    protected final VBox vB_Multas;
    private ArrayList<displayMultas> multas;

    public VistaMultas(Anschluss cdb) {
        
        this.cdb = cdb;
        vBox = new VBox();
        lbl_Multas = new Label();
        txt_Nombres = new HBox();
        lb_Nombres = new Label();
        tf_Nombres = new TextField();
        label1 = new Label();
        tf_Apelllidos = new TextField();
        lbl_Fecha = new Label();
        dt_fecha = new DatePicker();
        btn_Buscar = new Button();
        spMultas = new ScrollPane();
        vB_Multas = new VBox();

        setId("ap_Ancho");
        setPrefHeight(720.0);
        setPrefWidth(1150.0);
        setStyle("-fx-background-color: #F1FCED;");

        vBox.setId("vb_box");
        vBox.setPrefHeight(711.0);
        vBox.setPrefWidth(1205.0);

        lbl_Multas.setId("lbl_Multas");
        lbl_Multas.setPrefHeight(42.0);
        lbl_Multas.setPrefWidth(129.0);
        lbl_Multas.setText("Multas");
        lbl_Multas.setTextFill(javafx.scene.paint.Color.valueOf("#1728e4"));
        lbl_Multas.setTextOverrun(javafx.scene.control.OverrunStyle.CENTER_WORD_ELLIPSIS);
        lbl_Multas.setFont(new Font(24.0));

        txt_Nombres.setId("txt_Nombres");
        txt_Nombres.setPrefHeight(42.0);
        txt_Nombres.setPrefWidth(1161.0);

        lb_Nombres.setId("lb_Nombres");
        lb_Nombres.setText("Nombres:");
        lb_Nombres.setFont(new Font(18.0));

        tf_Nombres.setAlignment(javafx.geometry.Pos.TOP_LEFT);
        tf_Nombres.setEditable(false);
        tf_Nombres.setId("tf_Nombres");
        tf_Nombres.setPrefHeight(27.0);
        tf_Nombres.setPrefWidth(154.0);
        tf_Nombres.setOpaqueInsets(new Insets(0.0));

        label1.setText("Apellidos:");
        label1.setFont(new Font(18.0));

        tf_Apelllidos.setId("tf_Apelllidos");
        tf_Apelllidos.setPrefHeight(25.0);
        tf_Apelllidos.setPrefWidth(157.0);

        lbl_Fecha.setId("lbl_Fecha:");
        lbl_Fecha.setText("Fecha:");
        lbl_Fecha.setFont(new Font(18.0));

        dt_fecha.setId("dt_fecha");

        btn_Buscar.setId("btn_Buscar");
        btn_Buscar.setMnemonicParsing(false);
        btn_Buscar.setText("Buscar");
        txt_Nombres.setOpaqueInsets(new Insets(0.0));

        spMultas.setPrefHeight(631.0);
        spMultas.setPrefWidth(1208.0);

        vB_Multas.setPrefHeight(625.0);
        vB_Multas.setPrefWidth(1202.0);
        spMultas.setContent(vB_Multas);
        setPadding(new Insets(20.0));
        setOpaqueInsets(new Insets(0.0));

        
        poblar();
        asigBtn();

    }
    private void poblar(){
        vBox.getChildren().add(lbl_Multas);
        txt_Nombres.getChildren().add(lb_Nombres);
        txt_Nombres.getChildren().add(tf_Nombres);
        txt_Nombres.getChildren().add(label1);
        txt_Nombres.getChildren().add(tf_Apelllidos);
        txt_Nombres.getChildren().add(lbl_Fecha);
        txt_Nombres.getChildren().add(dt_fecha);
        txt_Nombres.getChildren().add(btn_Buscar);
        vBox.getChildren().add(txt_Nombres);
        vBox.getChildren().add(spMultas);
        getChildren().add(vBox);
        vB_Multas.getChildren().add(new displayMultas(cdb));
        vB_Multas.toFront();;
        
    }
    private void cargarMultas(){
            String nom;
            if(tf_Nombres.getText().equals("")){
                nom = "%";
              
            }else{
                nom = tf_Nombres.getText();
            }
            String ap;
            if(tf_Apelllidos.getText().equals("")){
                ap = "%";
              
            }else{
                ap = tf_Apelllidos.getText();
            }String f;
            if(dt_fecha.getValue()!= null){
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("uuuu-MM-dd");
                f = dt_fecha.getValue().format(formatter);
            }else{
                f= "%";
            }
            multas = cdb.getMultas(nom, ap, f);
            multas.forEach((rd) -> {
                vB_Multas.getChildren().add(rd);
            });
                    
        }
        private void asigBtn(){
        btn_Buscar.setOnAction((ActionEvent event) -> {
            vB_Multas.getChildren().clear();
            vB_Multas.getChildren().add(new displayMultas(cdb));
            cargarMultas();
        });
    }
}
