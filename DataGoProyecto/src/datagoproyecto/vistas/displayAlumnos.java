package datagoproyecto.vistas;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.scene.text.Font;

/**
 *
 * @author Carlos
 */
public class displayAlumnos extends HBox{
    
    protected final Label lbl_Cedula;
    protected final Label lbl_Nombre;
    protected final Label lbl_Apellido;
    protected final Label lbl_Fecha;
    protected final Label lbl_Telefono;
    protected final Label lbl_Nacionalidad;
    protected final Label lbl_ContactoEmer;
    protected final Label lbl_Direccion;
    protected final Label lbl_Representante;
    protected final Label lbl_Madre;
    protected final Label lbl_Padre;
    protected final Label lbl_Activo;
    
    public displayAlumnos(){
        lbl_Cedula = new Label();
        lbl_Nombre = new Label();
        lbl_Apellido = new Label();
        lbl_Fecha = new Label();
        lbl_Telefono = new Label();
        lbl_Nacionalidad = new Label();
        lbl_ContactoEmer = new Label();
        lbl_Direccion = new Label();
        lbl_Representante = new Label();
        lbl_Madre = new Label();
        lbl_Padre = new Label();
        lbl_Activo = new Label();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));
        
        lbl_Cedula.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Cedula.setId("lbl_Cedula");
        lbl_Cedula.setPrefWidth(150.0);
        lbl_Cedula.setText("Cedula");
        lbl_Cedula.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Nombre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Nombre.setId("lbl_Nombre");
        lbl_Nombre.setPrefWidth(150.0);
        lbl_Nombre.setText("Nombre");
        lbl_Nombre.setFont(new Font("Segoe UI Semilight", 14.0));
        HBox.setMargin(lbl_Nombre, new Insets(0.0, 0.0, 0.0, 5.0));

        lbl_Apellido.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Apellido.setId("lbl_Apellido");
        lbl_Apellido.setPrefWidth(150.0);
        lbl_Apellido.setText("Apellido");
        lbl_Apellido.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Fecha.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setPrefWidth(200.0);
        lbl_Fecha.setText("Fecha de Nacimiento");
        lbl_Fecha.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Telefono.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Telefono.setId("lbl_Telefono");
        lbl_Telefono.setPrefWidth(150.0);
        lbl_Telefono.setText("Telefono");
        lbl_Telefono.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Nacionalidad.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Nacionalidad.setId("lbl_Nacionalidad");
        lbl_Nacionalidad.setPrefWidth(150.0);
        lbl_Nacionalidad.setText("Nacionalidad");
        lbl_Nacionalidad.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_ContactoEmer.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_ContactoEmer.setId("lbl_ContactoEmer");
        lbl_ContactoEmer.setPrefWidth(125.0);
        lbl_ContactoEmer.setText("Contacto Emer.");
        lbl_ContactoEmer.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Direccion.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Direccion.setId("lbl_Direccion");
        lbl_Direccion.setPrefWidth(150.0);
        lbl_Direccion.setText("Direccion");
        lbl_Direccion.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Representante.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Representante.setId("lbl_Representante");
        lbl_Representante.setPrefWidth(150.0);
        lbl_Representante.setText("Representante");
        lbl_Representante.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Madre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Madre.setId("lbl_Madre");
        lbl_Madre.setPrefWidth(150.0);
        lbl_Madre.setText("Madre");
        lbl_Madre.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Padre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Padre.setId("lbl_Padre");
        lbl_Padre.setPrefWidth(150.0);
        lbl_Padre.setText("Padre");
        lbl_Padre.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Activo.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Activo.setId("lbl_Activo");
        lbl_Activo.setPrefWidth(150.0);
        lbl_Activo.setText("Activo");
        lbl_Activo.setFont(new Font("Segoe UI Semilight", 14.0));

        
        poblar();

    }
    public displayAlumnos(String cedula, String nom, String ape, String fecha, String tel,
            String nacio, String cont, String direc, String Repre, String madre, String padre, String activo) {

        lbl_Cedula = new Label();
        lbl_Nombre = new Label();
        lbl_Apellido = new Label();
        lbl_Fecha = new Label();
        lbl_Telefono = new Label();
        lbl_Nacionalidad = new Label();
        lbl_ContactoEmer = new Label();
        lbl_Direccion = new Label();
        lbl_Representante= new Label();
        lbl_Madre = new Label();
        lbl_Padre = new Label();
        lbl_Activo = new Label();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));
        
        lbl_Cedula.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Cedula.setId("lbl_Cedula");
        lbl_Cedula.setPrefWidth(150.0);
        lbl_Cedula.setText(cedula);
        lbl_Cedula.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Nombre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Nombre.setId("lbl_Nombre");
        lbl_Nombre.setPrefWidth(150.0);
        lbl_Nombre.setText(nom);
        lbl_Nombre.setFont(new Font("Segoe UI Semilight", 14.0));
        HBox.setMargin(lbl_Nombre, new Insets(0.0, 0.0, 0.0, 5.0));

        lbl_Apellido.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Apellido.setId("lbl_Apellido");
        lbl_Apellido.setPrefWidth(150.0);
        lbl_Apellido.setText(ape);
        lbl_Apellido.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Fecha.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setLayoutX(200.0);
        lbl_Fecha.setLayoutY(15.0);
        lbl_Fecha.setPrefWidth(100.0);
        lbl_Fecha.setText(fecha);
        lbl_Fecha.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Telefono.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Telefono.setId("lbl_Telefono");
        lbl_Telefono.setPrefWidth(150.0);
        lbl_Telefono.setText(tel);
        lbl_Telefono.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Nacionalidad.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Nacionalidad.setId("lbl_Nacionalidad");
        lbl_Nacionalidad.setPrefWidth(150.0);
        lbl_Nacionalidad.setText(nacio);
        lbl_Nacionalidad.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_ContactoEmer.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_ContactoEmer.setId("lbl_ContactoEmer");
        lbl_ContactoEmer.setPrefWidth(125.0);
        lbl_ContactoEmer.setText(cont);
        lbl_ContactoEmer.setFont(new Font("Segoe UI Semilight", 14.0));

        lbl_Direccion.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Direccion.setId("lbl_Direccion");
        lbl_Direccion.setPrefWidth(150.0);
        lbl_Direccion.setText(direc);
        lbl_Direccion.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Representante.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Representante.setId("lbl_Representante");
        lbl_Representante.setPrefWidth(150.0);
        lbl_Representante.setText(Repre);
        lbl_Representante.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Madre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Madre.setId("lbl_Madre");
        lbl_Madre.setPrefWidth(150.0);
        lbl_Madre.setText(madre);
        lbl_Madre.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Padre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Padre.setId("lbl_Padre");
        lbl_Padre.setPrefWidth(150.0);
        lbl_Padre.setText(padre);
        lbl_Padre.setFont(new Font("Segoe UI Semilight", 14.0));
        
        lbl_Activo.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Activo.setId("lbl_Activo");
        lbl_Activo.setPrefWidth(150.0);
        lbl_Activo.setText(activo);
        lbl_Activo.setFont(new Font("Segoe UI Semilight", 14.0));
        

        poblarTodo();
    }
    
    private void poblarTodo(){
        getChildren().add(lbl_Cedula);
        getChildren().add(lbl_Nombre);
        getChildren().add(lbl_Apellido);
        getChildren().add(lbl_Fecha);
        getChildren().add(lbl_Telefono);
        getChildren().add(lbl_Nacionalidad);
        getChildren().add(lbl_ContactoEmer);
        getChildren().add(lbl_Direccion);
        getChildren().add(lbl_Representante);
        getChildren().add(lbl_Madre);
        getChildren().add(lbl_Padre);
        getChildren().add(lbl_Activo);
    }
    
    private void poblar(){
        getChildren().add(lbl_Cedula);
        getChildren().add(lbl_Nombre);
        getChildren().add(lbl_Apellido);
        getChildren().add(lbl_Fecha);
        getChildren().add(lbl_Telefono);
        getChildren().add(lbl_Nacionalidad);
        getChildren().add(lbl_ContactoEmer);
        getChildren().add(lbl_Direccion);
        getChildren().add(lbl_Representante);
        getChildren().add(lbl_Madre);
        getChildren().add(lbl_Padre);
        getChildren().add(lbl_Activo);
    }
}