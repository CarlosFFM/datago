package datagoproyecto.vistas;

import datagoproyecto.conexion.Anschluss;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class VistaPagos extends AnchorPane {
    
    private Anschluss cdb;

    protected final VBox vBox;
    protected final Label lbl_Pagos;
    protected final HBox hBox;
    protected final Label lbl_Ced;
    protected final TextField tf_Ced;
    protected final Button btn_Buscar;
    protected final Button btn_Inpagos;
    protected final HBox hBox0;
    protected final Label lbl_Fecha;
    protected final DatePicker dp_Fecha;
    protected final ScrollPane SP_Pagos;
    protected final VBox vB_Pagos;
    private ArrayList<displayPagos> pagos;

    public VistaPagos(Anschluss cdb) {
        
        this.cdb = cdb;

        vBox = new VBox();
        lbl_Pagos = new Label();
        hBox = new HBox();
        lbl_Ced = new Label();
        tf_Ced = new TextField();
        btn_Buscar = new Button();
        btn_Inpagos = new Button();
        hBox0 = new HBox();
        lbl_Fecha = new Label();
        dp_Fecha = new DatePicker();
        SP_Pagos = new ScrollPane();
        vB_Pagos = new VBox();

        setId("AnchorPane");
        setPrefHeight(720.0);
        setPrefWidth(1150.0);
        setStyle("-fx-background-color: #F1FCED;");

        AnchorPane.setBottomAnchor(vBox, 0.0);
        AnchorPane.setLeftAnchor(vBox, 0.0);
        AnchorPane.setRightAnchor(vBox, 0.0);
        AnchorPane.setTopAnchor(vBox, 0.0);
        vBox.setPrefHeight(600.0);
        vBox.setPrefWidth(100.0);

        lbl_Pagos.setId("lbl_Pagos");
        lbl_Pagos.setText("Pagos");
        lbl_Pagos.setTextFill(javafx.scene.paint.Color.valueOf("#0060c0"));
        VBox.setMargin(lbl_Pagos, new Insets(5.0, 0.0, 5.0, 10.0));
        lbl_Pagos.setFont(new Font("Segoe UI Semilight", 20.0));

        hBox.setPrefWidth(200.0);

        lbl_Ced.setId("lbl_Ced");
        lbl_Ced.setText("Cedula: ");
        lbl_Ced.setFont(new Font(18.0));
        HBox.setMargin(lbl_Ced, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_Ced.setId("tf_Ced");
        tf_Ced.setPromptText("Cedula");
        HBox.setMargin(tf_Ced, new Insets(0.0, 5.0, 0.0, 0.0));

        btn_Buscar.setId("btn_Buscar");
        btn_Buscar.setMnemonicParsing(false);
        btn_Buscar.setText("Buscar");
        HBox.setMargin(btn_Buscar, new Insets(0.0, 5.0, 0.0, 0.0));
        hBox.setPadding(new Insets(5.0, 0.0, 5.0, 10.0));
        
        btn_Inpagos.setId("btn_Inpagos");
        btn_Inpagos.setMnemonicParsing(false);
        btn_Inpagos.setText("Inpagos");
        HBox.setMargin(btn_Inpagos, new Insets(0.0, 5.0, 0.0, 0.0));
        hBox.setPadding(new Insets(5.0, 0.0, 5.0, 10.0));

        hBox0.setPrefWidth(200.0);

        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setText("Fecha:");
        lbl_Fecha.setFont(new Font(18.0));
        HBox.setMargin(lbl_Fecha, new Insets(0.0, 5.0, 0.0, 0.0));

        dp_Fecha.setId("dp_Fecha");
        dp_Fecha.setPromptText("Fecha del pago");
        hBox0.setPadding(new Insets(5.0, 0.0, 5.0, 10.0));

        SP_Pagos.setId("SP_Pagos");
        SP_Pagos.setPrefHeight(610.0);
        SP_Pagos.setPrefWidth(200.0);

        vB_Pagos.setId("vB_Pagos");
        vB_Pagos.setPrefWidth(1150.0);
        SP_Pagos.setContent(vB_Pagos);

        asigBtn();
        poblar();
    }
    
    private void poblar(){
        vBox.getChildren().add(lbl_Pagos);
        hBox.getChildren().add(lbl_Ced);
        hBox.getChildren().add(tf_Ced);
        hBox.getChildren().add(btn_Buscar);
        hBox.getChildren().add(btn_Inpagos);
        vBox.getChildren().add(hBox);
        hBox0.getChildren().add(lbl_Fecha);
        hBox0.getChildren().add(dp_Fecha);
        vBox.getChildren().add(hBox0);
        vBox.getChildren().add(SP_Pagos);
        this.getChildren().add(vBox);
        vB_Pagos.getChildren().add(new displayPagos(cdb));
        vB_Pagos.toFront();
    }
    
    private void asigBtn(){
        btn_Buscar.setOnAction((ActionEvent event) -> {
            vB_Pagos.getChildren().clear();
            vB_Pagos.getChildren().add(new displayPagos(cdb));
            cargarPagos();
        });
        
        btn_Inpagos.setOnAction((ActionEvent event) -> {
            vB_Pagos.getChildren().clear();
            vB_Pagos.getChildren().add(new displayPagos(cdb));
            cargarInpagos();
        });
    }
    
    private void cargarPagos(){
        String ced;
        if (tf_Ced.getText().equals("")){
            ced = "%";
        } else { 
            ced = tf_Ced.getText();
        }
        
        String f;
        if (dp_Fecha.getValue() != null){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-uuuu");
            f = dp_Fecha.getValue().format(formatter);
        } else {
            f = "%";
        }
        
        pagos = cdb.getPagos(ced, f);
        pagos.forEach((rdp) -> {
            vB_Pagos.getChildren().add(rdp);
        });
    }
    
    private void cargarInpagos(){
        Calendar cal = Calendar.getInstance();
        int mes = cal.get(Calendar.MONTH) + 1;
        if (mes == 1){
            cargarInpagos(mes);
        }
        for (int m = 4; m <= 12; m++){
            cargarInpagos(m);
        }
    }
    
    private void cargarInpagos(int mes){
        String month =  String.valueOf(mes);
        pagos = cdb.getInpagos(month);
        pagos.forEach((rdp) -> {
            vB_Pagos.getChildren().add(rdp);
        });
    }
}
