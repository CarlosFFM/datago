
package datagoproyecto.vistas;
import datagoproyecto.conexion.Anschluss;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class displayMultas extends HBox {

    private Anschluss cdb;
    protected final Label lbl_Nombre;
    protected final Label lbl_Apellido;
    protected final Label lbl_Valor;
    protected final Label lbl_Descripcion;
    protected final Label lbl_Fecha;
    protected final Label lbl_Hora;
    protected final Label lbl_Cargo;

    public displayMultas(Anschluss cdb) {
        this.cdb = cdb;
        lbl_Nombre = new Label();
        lbl_Apellido = new Label();
        lbl_Valor = new Label();
        lbl_Descripcion = new Label();
        lbl_Fecha = new Label();
        lbl_Hora = new Label();
        lbl_Cargo = new Label();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));

        lbl_Nombre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Nombre.setId("lbl_Nombre");
        lbl_Nombre.setPrefWidth(200.0);
        lbl_Nombre.setText("Nombre");
        lbl_Nombre.setFont(new Font("Segoe UI Semilight", 16.0));
        HBox.setMargin(lbl_Nombre, new Insets(0.0, 0.0, 0.0, 5.0));

        lbl_Apellido.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Apellido.setId("lbl_Apellido");
        lbl_Apellido.setPrefWidth(200.0);
        lbl_Apellido.setText("Apellido");
        lbl_Apellido.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Valor.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Valor.setId("lbl_Valor");
        lbl_Valor.setPrefWidth(75.0);
        lbl_Valor.setText("Valor");
        lbl_Valor.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Descripcion.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Descripcion.setId("lbl_Descripcion");
        lbl_Descripcion.setPrefHeight(25.0);
        lbl_Descripcion.setPrefWidth(260.0);
        lbl_Descripcion.setText("Descripcion");
        lbl_Descripcion.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Fecha.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setLayoutX(215.0);
        lbl_Fecha.setLayoutY(15.0);
        lbl_Fecha.setPrefHeight(25.0);
        lbl_Fecha.setPrefWidth(112.0);
        lbl_Fecha.setText("Fecha");
        lbl_Fecha.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Hora.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Hora.setId("lbl_Hora");
        lbl_Hora.setPrefWidth(125.0);
        lbl_Hora.setText("Hora");
        lbl_Hora.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Cargo.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Cargo.setId("lbl_Cargo");
        lbl_Cargo.setPrefWidth(125.0);
        lbl_Cargo.setText("Cargo");
        lbl_Cargo.setFont(new Font("Segoe UI Semilight", 16.0));

        poblar();

    }
    public displayMultas(String nombre, String apellido, String valor , String descripcion, String fecha,
            String hora, String cargo,Anschluss cdb){
        this.cdb = cdb;
        lbl_Nombre = new Label();
        lbl_Apellido = new Label();
        lbl_Valor = new Label();
        lbl_Descripcion = new Label();
        lbl_Fecha = new Label();
        lbl_Hora = new Label();
        lbl_Cargo = new Label();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));

        lbl_Nombre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Nombre.setId("lbl_Nombre");
        lbl_Nombre.setPrefWidth(200.0);
        lbl_Nombre.setText(nombre);
        lbl_Nombre.setFont(new Font("Segoe UI Semilight", 16.0));
        HBox.setMargin(lbl_Nombre, new Insets(0.0, 0.0, 0.0, 5.0));

        lbl_Apellido.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Apellido.setId("lbl_Apellido");
        lbl_Apellido.setPrefWidth(200.0);
        lbl_Apellido.setText(apellido);
        lbl_Apellido.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Valor.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Valor.setId("lbl_Valor");
        lbl_Valor.setPrefWidth(75.0);
        lbl_Valor.setText(valor);
        lbl_Valor.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Descripcion.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Descripcion.setId("lbl_Descripcion");
        lbl_Descripcion.setPrefHeight(25.0);
        lbl_Descripcion.setPrefWidth(260.0);
        lbl_Descripcion.setText(descripcion);
        lbl_Descripcion.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Fecha.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setLayoutX(215.0);
        lbl_Fecha.setLayoutY(15.0);
        lbl_Fecha.setPrefHeight(25.0);
        lbl_Fecha.setPrefWidth(112.0);
        lbl_Fecha.setText(fecha);
        lbl_Fecha.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Hora.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Hora.setId("lbl_Hora");
        lbl_Hora.setPrefWidth(125.0);
        lbl_Hora.setText(hora);
        lbl_Hora.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Cargo.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Cargo.setId("lbl_Cargo");
        lbl_Cargo.setPrefWidth(125.0);
        lbl_Cargo.setText(cargo);
        lbl_Cargo.setFont(new Font("Segoe UI Semilight", 16.0));

        poblar();
        
        
        
        
    }
    private void poblar(){
        getChildren().add(lbl_Nombre);
        getChildren().add(lbl_Apellido);
        getChildren().add(lbl_Valor);
        getChildren().add(lbl_Descripcion);
        getChildren().add(lbl_Fecha);
        getChildren().add(lbl_Hora);
        getChildren().add(lbl_Cargo);
        
    }
}
