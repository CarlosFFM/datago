package datagoproyecto.vistas;

import datagoproyecto.MainUI;
import datagoproyecto.conexion.Anschluss;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;

public class ControlPanel extends AnchorPane {

    protected final AnchorPane sideBar;
    protected final Button btn_Alum;
    protected final Button btn_Mat;
    protected final Button btn_Mul;
    protected final Button btn_Pag;
    protected final Button btn_Profs;
    protected final Button btn_CS;
    private AnchorPane PaneRotativo;
    private Anschluss cdb;
    private MainUI main;

    public ControlPanel(Anschluss cdb, MainUI main) {
        
        this.main = main;
        this.cdb = cdb;

        sideBar = new AnchorPane();
        btn_Alum = new Button();
        btn_Mat = new Button();
        btn_Mul = new Button();
        btn_Pag = new Button();
        btn_Profs = new Button();
        btn_CS = new Button();
        PaneRotativo = new AnchorPane();
        
        createSideBar();
        populate();
        asigBtn();
    }
    
    private void populate(){
        setId("AnchorPane");
        setPrefHeight(720.0);
        setPrefWidth(1280.0);
        setStyle("-fx-background-color: #f1fced;");

        getChildren().add(sideBar);
        getChildren().add(PaneRotativo);
    }
    
    private void createSideBar(){
        AnchorPane.setBottomAnchor(sideBar, 0.0);
        AnchorPane.setLeftAnchor(sideBar, 0.0);
        AnchorPane.setTopAnchor(sideBar, 0.0);
        sideBar.setLayoutX(140.0);
        sideBar.setLayoutY(-21.0);
        sideBar.setPrefHeight(720.0);
        sideBar.setPrefWidth(130.0);
        sideBar.setStyle("-fx-background-color: #dce6d8;");

        AnchorPane.setLeftAnchor(btn_Alum, 0.0);
        AnchorPane.setRightAnchor(btn_Alum, 0.0);
        AnchorPane.setTopAnchor(btn_Alum, 0.0);
        btn_Alum.setId("btn_Alum");
        btn_Alum.setMnemonicParsing(false);
        btn_Alum.setPrefHeight(120.0);
        btn_Alum.setStyle("-fx-background-color: #0069d3;");
        btn_Alum.setText("Alumnos");
        btn_Alum.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        btn_Alum.setTextFill(javafx.scene.paint.Color.valueOf("#f1fced"));
        btn_Alum.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setLeftAnchor(btn_Mat, 0.0);
        AnchorPane.setRightAnchor(btn_Mat, 0.0);
        AnchorPane.setTopAnchor(btn_Mat, 120.0);
        btn_Mat.setId("btn_Mat");
        btn_Mat.setLayoutY(150.0);
        btn_Mat.setMnemonicParsing(false);
        btn_Mat.setPrefHeight(120.0);
        btn_Mat.setStyle("-fx-background-color: #2fd123;");
        btn_Mat.setText("Matriculacion");
        btn_Mat.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        btn_Mat.setTextFill(javafx.scene.paint.Color.valueOf("#f1fced"));
        btn_Mat.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setLeftAnchor(btn_Mul, 0.0);
        AnchorPane.setRightAnchor(btn_Mul, 0.0);
        AnchorPane.setTopAnchor(btn_Mul, 240.0);
        btn_Mul.setId("btn_Mul");
        btn_Mul.setLayoutY(300.0);
        btn_Mul.setMnemonicParsing(false);
        btn_Mul.setPrefHeight(120.0);
        btn_Mul.setStyle("-fx-background-color: #fcf06f;");
        btn_Mul.setText("Multas");
        btn_Mul.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        btn_Mul.setTextFill(javafx.scene.paint.Color.valueOf("#0069d3"));
        btn_Mul.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setLeftAnchor(btn_Pag, 0.0);
        AnchorPane.setRightAnchor(btn_Pag, 0.0);
        AnchorPane.setTopAnchor(btn_Pag, 360.0);
        btn_Pag.setId("btn_Pag");
        btn_Pag.setLayoutY(450.0);
        btn_Pag.setMnemonicParsing(false);
        btn_Pag.setPrefHeight(120.0);
        btn_Pag.setStyle("-fx-background-color: #fc4214;");
        btn_Pag.setText("Pagos");
        btn_Pag.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        btn_Pag.setTextFill(javafx.scene.paint.Color.valueOf("#f1fced"));
        btn_Pag.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setLeftAnchor(btn_Profs, 0.0);
        AnchorPane.setRightAnchor(btn_Profs, 0.0);
        AnchorPane.setTopAnchor(btn_Profs, 480.0);
        btn_Profs.setId("btn_Profs");
        btn_Profs.setLayoutY(600.0);
        btn_Profs.setMnemonicParsing(false);
        btn_Profs.setPrefHeight(120.0);
        btn_Profs.setPrefWidth(130.0);
        btn_Profs.setStyle("-fx-background-color: #0069d3;");
        btn_Profs.setText("Profesores");
        btn_Profs.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        btn_Profs.setTextFill(javafx.scene.paint.Color.valueOf("#f1fced"));
        btn_Profs.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setBottomAnchor(btn_CS, 0.0);
        AnchorPane.setLeftAnchor(btn_CS, 0.0);
        AnchorPane.setRightAnchor(btn_CS, 0.0);
        AnchorPane.setTopAnchor(btn_CS, 600.0);
        btn_CS.setId("btn_CS");
        btn_CS.setLayoutX(5.0);
        btn_CS.setLayoutY(593.0);
        btn_CS.setMnemonicParsing(false);
        btn_CS.setPrefHeight(120.0);
        btn_CS.setStyle("-fx-background-color: #2fd123;");
        btn_CS.setText("Cerrar Sesion");
        btn_CS.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        btn_CS.setTextFill(javafx.scene.paint.Color.valueOf("#f1fced"));
        btn_CS.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setBottomAnchor(PaneRotativo, 0.0);
        AnchorPane.setLeftAnchor(PaneRotativo, 130.0);
        AnchorPane.setRightAnchor(PaneRotativo, 0.0);
        AnchorPane.setTopAnchor(PaneRotativo, 0.0);
        PaneRotativo.setId("PaneRotativo");
        PaneRotativo.setLayoutX(511.0);
        PaneRotativo.setLayoutY(136.0);
        PaneRotativo.setPrefHeight(200.0);
        PaneRotativo.setPrefWidth(200.0);

        sideBar.getChildren().add(btn_Alum);
        sideBar.getChildren().add(btn_Mat);
        sideBar.getChildren().add(btn_Mul);
        sideBar.getChildren().add(btn_Pag);
        sideBar.getChildren().add(btn_Profs);
        sideBar.getChildren().add(btn_CS);
    }
    
    private void changePane(AnchorPane otro){
        AnchorPane.setBottomAnchor(otro, 0.0);
        AnchorPane.setLeftAnchor(otro, 130.0);
        AnchorPane.setRightAnchor(otro, 0.0);
        AnchorPane.setTopAnchor(otro, 0.0);
        otro.setLayoutX(0);
        otro.setLayoutY(0);
        otro.setPrefHeight(200.0);
        otro.setPrefWidth(200.0);
        Platform.runLater(() -> {
            this.getChildren().remove(PaneRotativo);
            PaneRotativo = otro;
            this.getChildren().add(PaneRotativo);
            PaneRotativo.toFront();
        });
    }
    
    private void asigBtn(){
        btn_Alum.setOnAction((ActionEvent event) -> {
            changePane(new VistaAlumnos(cdb));
        });
        
        btn_Mat.setOnAction((ActionEvent event) -> {
            changePane(new VistaMat(cdb));
        });
        
        btn_Mul.setOnAction((ActionEvent event) -> {
            changePane(new VistaMultas(cdb));
        });
        
        btn_Pag.setOnAction((ActionEvent event) -> {
            changePane(new VistaPagos(cdb));
        });
        
        btn_Profs.setOnAction((ActionEvent event) -> {
            changePane(new VistaProfs(cdb));
        });
        
        btn_CS.setOnAction((ActionEvent event) -> {
            cdb.close();
            cdb = null;
            main.changeUI(new Login(main));
        });
    }

}
