package datagoproyecto.vistas;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class displayProfs extends HBox {

    protected final Label nom;
    protected final Label ape;
    protected final Label caps;
    protected final Label Materia;
    protected final Label niv;
    protected final Label Multas;

    public displayProfs(String nom, String ape, String caps, String Materia, String niv, String Multas) {

        this.nom = new Label();
        this.ape = new Label();
        this.caps = new Label();
        this.Materia = new Label();
        this.niv = new Label();
        this.Multas = new Label();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));

        this.nom.setAlignment(javafx.geometry.Pos.CENTER);
        this.nom.setId("lbl_Nombre");
        this.nom.setPrefWidth(200.0);
        this.nom.setText(nom);
        this.nom.setFont(new Font("Segoe UI Semilight", 16.0));
        HBox.setMargin(this.nom, new Insets(0.0, 0.0, 0.0, 5.0));

        this.ape.setAlignment(javafx.geometry.Pos.CENTER);
        this.ape.setId("lbl_Apellido");
        this.ape.setPrefWidth(200.0);
        this.ape.setText(ape);
        this.ape.setFont(new Font("Segoe UI Semilight", 16.0));

        this.caps.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        this.caps.setId("lbl_Fecha");
        this.caps.setLayoutX(215.0);
        this.caps.setLayoutY(15.0);
        this.caps.setPrefHeight(26.0);
        this.caps.setPrefWidth(400.0);
        this.caps.setText(caps);
        this.caps.setFont(new Font("Segoe UI Semilight", 16.0));

        this.Materia.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        this.Materia.setId("lbl_Valor");
        this.Materia.setPrefWidth(100.0);
        this.Materia.setText(Materia);
        this.Materia.setFont(new Font("Segoe UI Semilight", 16.0));

        this.niv.setAlignment(javafx.geometry.Pos.CENTER);
        this.niv.setId("lbl_Estado");
        this.niv.setPrefWidth(100.0);
        this.niv.setText(niv);
        this.niv.setFont(new Font("Segoe UI Semilight", 16.0));

        this.Multas.setAlignment(javafx.geometry.Pos.CENTER);
        this.Multas.setId("lbl_Descripcion");
        this.Multas.setPrefWidth(100.0);
        this.Multas.setText(Multas);
        this.Multas.setFont(new Font("Segoe UI Semilight", 16.0));

        getChildren().add(this.nom);
        getChildren().add(this.ape);
        getChildren().add(this.caps);
        getChildren().add(this.Materia);
        getChildren().add(this.niv);
        getChildren().add(this.Multas);

    }
}
