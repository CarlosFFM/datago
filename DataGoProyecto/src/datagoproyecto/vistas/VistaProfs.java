package datagoproyecto.vistas;

import datagoproyecto.conexion.Anschluss;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class VistaProfs extends AnchorPane {

    protected final VBox vBox;
    protected final Label lbl_Pagos;
    protected final HBox hBox;
    protected final Label lbl_Ced;
    protected final TextField tf_Ced;
    protected final HBox hBox0;
    protected final Label lbl_Nivel;
    protected final TextField tf_Nivel;
    protected final Button btn_Buscar;
    protected final Button btn_Multas;
    protected final ScrollPane SP_Profs;
    protected final VBox vb_Profs;
    private ArrayList<displayProfs> profs;
    private Anschluss cdb;

    public VistaProfs(Anschluss cdb) {
        
        this.cdb = cdb;

        vBox = new VBox();
        lbl_Pagos = new Label();
        hBox = new HBox();
        lbl_Ced = new Label();
        tf_Ced = new TextField();
        hBox0 = new HBox();
        lbl_Nivel = new Label();
        tf_Nivel = new TextField();
        btn_Buscar = new Button();
        btn_Multas = new Button();
        SP_Profs = new ScrollPane();
        vb_Profs = new VBox();

        setId("AnchorPane");
        setPrefHeight(720.0);
        setPrefWidth(1150.0);
        setStyle("-fx-background-color: #F1FCED;");

        AnchorPane.setBottomAnchor(vBox, 0.0);
        AnchorPane.setLeftAnchor(vBox, 0.0);
        AnchorPane.setRightAnchor(vBox, 0.0);
        AnchorPane.setTopAnchor(vBox, 0.0);
        vBox.setPrefHeight(600.0);
        vBox.setPrefWidth(100.0);

        lbl_Pagos.setId("lbl_Pagos");
        lbl_Pagos.setText("Profesores");
        lbl_Pagos.setTextFill(javafx.scene.paint.Color.valueOf("#0060c0"));
        VBox.setMargin(lbl_Pagos, new Insets(5.0, 0.0, 5.0, 10.0));
        lbl_Pagos.setFont(new Font("Segoe UI Semilight", 20.0));

        hBox.setPrefWidth(200.0);

        lbl_Ced.setId("lbl_Ced");
        lbl_Ced.setText("Cedula: ");
        lbl_Ced.setFont(new Font(18.0));
        HBox.setMargin(lbl_Ced, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_Ced.setId("tf_Ced");
        tf_Ced.setPromptText("Cedula");
        HBox.setMargin(tf_Ced, new Insets(0.0, 5.0, 0.0, 0.0));
        hBox.setPadding(new Insets(5.0, 0.0, 5.0, 10.0));

        hBox0.setPrefWidth(200.0);

        lbl_Nivel.setId("lbl_Nivel");
        lbl_Nivel.setText("Nivel: ");
        lbl_Nivel.setFont(new Font(18.0));
        HBox.setMargin(lbl_Nivel, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_Nivel.setId("tf_Nivel");
        tf_Nivel.setPromptText("Nivel");
        HBox.setMargin(tf_Nivel, new Insets(0.0, 5.0, 0.0, 0.0));

        btn_Buscar.setId("btn_Buscar");
        btn_Buscar.setMnemonicParsing(false);
        btn_Buscar.setText("Buscar");
        HBox.setMargin(btn_Buscar, new Insets(0.0, 5.0, 0.0, 0.0));

        btn_Multas.setId("btn_Multas");
        btn_Multas.setMnemonicParsing(false);
        btn_Multas.setText("Multas");
        hBox0.setPadding(new Insets(5.0, 0.0, 5.0, 10.0));

        SP_Profs.setId("SP_Profs");
        SP_Profs.setPrefHeight(610.0);
        SP_Profs.setPrefWidth(200.0);

        vb_Profs.setPrefWidth(1150.0);
        SP_Profs.setContent(vb_Profs);

        vBox.getChildren().add(lbl_Pagos);
        hBox.getChildren().add(lbl_Ced);
        hBox.getChildren().add(tf_Ced);
        vBox.getChildren().add(hBox);
        hBox0.getChildren().add(lbl_Nivel);
        hBox0.getChildren().add(tf_Nivel);
        hBox0.getChildren().add(btn_Buscar);
        hBox0.getChildren().add(btn_Multas);
        vBox.getChildren().add(hBox0);
        vBox.getChildren().add(SP_Profs);
        getChildren().add(vBox);
        
        asigBtn();
    }
    
    private void asigBtn(){
        btn_Buscar.setOnAction((ActionEvent event) -> {
            vb_Profs.getChildren().clear();
            vb_Profs.getChildren().add(new displayPagos(cdb));
            cargarProfes();
        });
    }
    
    private void cargarProfes(){
        String ced;
        if (tf_Ced.getText().equals("")){
            ced = "%";
        } else { 
            ced = tf_Ced.getText();
        }
        
        String niv;
        if (tf_Nivel.getText().equals("")){
            niv = "%";
        } else { 
            niv = tf_Nivel.getText();
        }
        
        profs = cdb.getProfs(ced);
        profs.forEach((rdp) -> {
            vb_Profs.getChildren().add(rdp);
        });
    }
}
