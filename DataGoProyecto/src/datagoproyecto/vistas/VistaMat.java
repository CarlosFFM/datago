package datagoproyecto.vistas;

import datagoproyecto.conexion.Anschluss;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class VistaMat extends AnchorPane {
    
    private final Anschluss cdb;

    private final Label lbl_Errores;
    private final ScrollPane scrollPane;
    private final AnchorPane APinSP_Mat;
    private final VBox VB_MainMat;
    private final Label lbl_DatosAlum;
    private final HBox HB_NomApeCedFNAlum;
    private final Label lbl_NombreAlum;
    private final TextField tf_NombreAlum;
    private final Label lbl_ApeAlum;
    private final TextField tf_ApeAlum;
    private final Label lbl_CedAlum;
    private final TextField tf_CedAlum;
    private final Label lbl_FNAlum;
    private final DatePicker dp_FNAlum;
    private final HBox HB_NacTelCEAlum;
    private final Label lbl_NacAlum;
    private final TextField tf_NacAlum;
    private final Label lbl_TelCasaAlum;
    private final TextField tf_TelCasaAlum;
    private final Label lbl_CEAlum;
    private final TextField tf_CEAlum;
    private final HBox HB_DirAlum;
    private final Label lbl_DirAlum;
    private final TextField tf_DirAlum;
    private final Label lbl_DatosPadre;
    private final HBox HB_NomApeCedFNPadre;
    private final Label lbl_NombrePadre;
    private final TextField tf_NombrePadre;
    private final Label lbl_ApePadre;
    private final TextField tf_ApePadre;
    private final Label lbl_CedPadre;
    private final TextField tf_CedPadre;
    private final Label lbl_FNPadre;
    private final DatePicker dp_FNPadre;
    private final HBox HB_NacCelNEPadre;
    private final Label lbl_NacPadre;
    private final TextField tf_NacPadre;
    private final Label lbl_CelPadre;
    private final TextField tf_CelPadre;
    private final Label lbl_NEPadre;
    private final TextField tf_NEPadre;
    private final HBox HB_ApRECVcEPadre;
    private final Label lbl_ApRPadre;
    private final ChoiceBox cb_ApRPadre;
    private final Label lbl_ECPadre;
    private final ChoiceBox cb_ECPadre;
    private final Label lbl_VcEPadre;
    private final ChoiceBox cb_VcEPadre;
    private final HBox HB_EmailOcuPadre;
    private final Label lbl_EmailPadre;
    private final TextField tf_EmailPadre;
    private final Label lbl_OcupacionPadre;
    private final TextField tf_OcupacionPadre;
    private final Label lbl_DatosMadre;
    private final HBox HB_NomApeCedFNMadre;
    private final Label lbl_NombreMadre;
    private final TextField tf_NombreMadre;
    private final Label lbl_ApeMadre;
    private final TextField tf_ApeMadre;
    private final Label lbl_CedMadre;
    private final TextField tf_CedMadre;
    private final Label lbl_FNMadre;
    private final DatePicker dp_FNMadre;
    private final HBox HB_NacCelNEMadre;
    private final Label lbl_NacMadre;
    private final TextField tf_NacMadre;
    private final Label lbl_CelMadre;
    private final TextField tf_CelMadre;
    private final Label lbl_NEMadre;
    private final TextField tf_NEMadre;
    private final HBox HB_ApRECVcEMadre;
    private final Label lbl_ApRMadre;
    private final ChoiceBox cb_ApRMadre;
    private final Label lbl_ECMadre;
    private final ChoiceBox cb_ECMadre;
    private final Label lbl_VcEMadre;
    private final ChoiceBox cb_VcEMadre;
    private final HBox HB_EmailOcuMadre;
    private final Label lbl_EmailMadre;
    private final TextField tf_EmailMadre;
    private final Label lbl_OcupacionMadre;
    private final TextField tf_OcupacionMadre;
    private final Label lbl_DatosRep;
    private final HBox HB_NomApeCedFNRep;
    private final Label lbl_NombreRep;
    private final TextField tf_NombreRep;
    private final Label lbl_ApeRep;
    private final TextField tf_ApeRep;
    private final Label lbl_CedRep;
    private final TextField tf_CedRep;
    private final Label lbl_FNRep;
    private final DatePicker dp_FNRep;
    private final HBox HB_NacCelNERep;
    private final Label lbl_NacRep;
    private final TextField tf_NacRep;
    private final Label lbl_CelRep;
    private final TextField tf_CelRep;
    private final Label lbl_NERep;
    private final TextField tf_NERep;
    private final HBox HB_ApRECVcERep;
    private final Label lbl_ApRRep;
    private final ChoiceBox cb_ApRRep;
    private final Label lbl_ECRep;
    private final ChoiceBox cb_ECRep;
    private final Label lbl_VcERep;
    private final ChoiceBox cb_VcERep;
    private final HBox HB_EmailOcuRep;
    private final Label lbl_EmailRep;
    private final TextField tf_EmailRep;
    private final Label lbl_OcupacionRep;
    private final TextField tf_OcupacionRep;
    private final HBox HB_Botones;
    private final Button btn_Matricular;
    private final Button btn_Cancelar;

    public VistaMat(Anschluss cdb) {
        
        this.cdb = cdb;

        lbl_Errores = new Label();
        scrollPane = new ScrollPane();
        APinSP_Mat = new AnchorPane();
        VB_MainMat = new VBox();
        lbl_DatosAlum = new Label();
        HB_NomApeCedFNAlum = new HBox();
        lbl_NombreAlum = new Label();
        tf_NombreAlum = new TextField();
        lbl_ApeAlum = new Label();
        tf_ApeAlum = new TextField();
        lbl_CedAlum = new Label();
        tf_CedAlum = new TextField();
        lbl_FNAlum = new Label();
        dp_FNAlum = new DatePicker();
        HB_NacTelCEAlum = new HBox();
        lbl_NacAlum = new Label();
        tf_NacAlum = new TextField();
        lbl_TelCasaAlum = new Label();
        tf_TelCasaAlum = new TextField();
        lbl_CEAlum = new Label();
        tf_CEAlum = new TextField();
        HB_DirAlum = new HBox();
        lbl_DirAlum = new Label();
        tf_DirAlum = new TextField();
        lbl_DatosPadre = new Label();
        HB_NomApeCedFNPadre = new HBox();
        lbl_NombrePadre = new Label();
        tf_NombrePadre = new TextField();
        lbl_ApePadre = new Label();
        tf_ApePadre = new TextField();
        lbl_CedPadre = new Label();
        tf_CedPadre = new TextField();
        lbl_FNPadre = new Label();
        dp_FNPadre = new DatePicker();
        HB_NacCelNEPadre = new HBox();
        lbl_NacPadre = new Label();
        tf_NacPadre = new TextField();
        lbl_CelPadre = new Label();
        tf_CelPadre = new TextField();
        lbl_NEPadre = new Label();
        tf_NEPadre = new TextField();
        HB_ApRECVcEPadre = new HBox();
        lbl_ApRPadre = new Label();
        cb_ApRPadre = new ChoiceBox();
        lbl_ECPadre = new Label();
        cb_ECPadre = new ChoiceBox();
        lbl_VcEPadre = new Label();
        cb_VcEPadre = new ChoiceBox();
        HB_EmailOcuPadre = new HBox();
        lbl_EmailPadre = new Label();
        tf_EmailPadre = new TextField();
        lbl_OcupacionPadre = new Label();
        tf_OcupacionPadre = new TextField();
        lbl_DatosMadre = new Label();
        HB_NomApeCedFNMadre = new HBox();
        lbl_NombreMadre = new Label();
        tf_NombreMadre = new TextField();
        lbl_ApeMadre = new Label();
        tf_ApeMadre = new TextField();
        lbl_CedMadre = new Label();
        tf_CedMadre = new TextField();
        lbl_FNMadre = new Label();
        dp_FNMadre = new DatePicker();
        HB_NacCelNEMadre = new HBox();
        lbl_NacMadre = new Label();
        tf_NacMadre = new TextField();
        lbl_CelMadre = new Label();
        tf_CelMadre = new TextField();
        lbl_NEMadre = new Label();
        tf_NEMadre = new TextField();
        HB_ApRECVcEMadre = new HBox();
        lbl_ApRMadre = new Label();
        cb_ApRMadre = new ChoiceBox();
        lbl_ECMadre = new Label();
        cb_ECMadre = new ChoiceBox();
        lbl_VcEMadre = new Label();
        cb_VcEMadre = new ChoiceBox();
        HB_EmailOcuMadre = new HBox();
        lbl_EmailMadre = new Label();
        tf_EmailMadre = new TextField();
        lbl_OcupacionMadre = new Label();
        tf_OcupacionMadre = new TextField();
        lbl_DatosRep = new Label();
        HB_NomApeCedFNRep = new HBox();
        lbl_NombreRep = new Label();
        tf_NombreRep = new TextField();
        lbl_ApeRep = new Label();
        tf_ApeRep = new TextField();
        lbl_CedRep = new Label();
        tf_CedRep = new TextField();
        lbl_FNRep = new Label();
        dp_FNRep = new DatePicker();
        HB_NacCelNERep = new HBox();
        lbl_NacRep = new Label();
        tf_NacRep = new TextField();
        lbl_CelRep = new Label();
        tf_CelRep = new TextField();
        lbl_NERep = new Label();
        tf_NERep = new TextField();
        HB_ApRECVcERep = new HBox();
        lbl_ApRRep = new Label();
        cb_ApRRep = new ChoiceBox();
        lbl_ECRep = new Label();
        cb_ECRep = new ChoiceBox();
        lbl_VcERep = new Label();
        cb_VcERep = new ChoiceBox();
        HB_EmailOcuRep = new HBox();
        lbl_EmailRep = new Label();
        tf_EmailRep = new TextField();
        lbl_OcupacionRep = new Label();
        tf_OcupacionRep = new TextField();
        HB_Botones = new HBox();
        btn_Matricular = new Button();
        btn_Cancelar = new Button();
        
        defInternos();
        poblar();
        llenarCBs();
        asigBtns();

    }
    
    private void llenarCBs(){
        ObservableList<String> aprItems = FXCollections.
                observableArrayList("Autorizado", "No Autorizado");
        ObservableList<String> ecItems = FXCollections.
                observableArrayList("Casado/a", "Soltero/a", "Divorciado/a",
                        "Viudo/a", "Union Lbre", "Comprometido/a","Separado/a");
        ObservableList<String> vceItems = FXCollections.
                observableArrayList("Vive", "No vive");
        
        this.cb_ApRPadre.setItems(aprItems);
        this.cb_ECPadre.setItems(ecItems);
        this.cb_VcEPadre.setItems(vceItems);
        this.cb_ApRRep.setItems(aprItems);
        this.cb_ECRep.setItems(ecItems);
        this.cb_VcERep.setItems(vceItems);
        this.cb_ApRMadre.setItems(aprItems);
        this.cb_ECMadre.setItems(ecItems);
        this.cb_VcEMadre.setItems(vceItems);
    }
    
    private void defInternos(){
                setId("AnchorPane");
        setPrefHeight(720.0);
        setPrefWidth(1150.0);
        setStyle("-fx-background-color: #F1FCED;");

        AnchorPane.setLeftAnchor(lbl_Errores, 0.0);
        AnchorPane.setTopAnchor(lbl_Errores, 0.0);
        lbl_Errores.setId("lbl_Errores");
        lbl_Errores.setText("Advertencia!");
        lbl_Errores.setTextFill(javafx.scene.paint.Color.valueOf("#fc4214"));
        lbl_Errores.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_Errores.setPadding(new Insets(5.0, 0.0, 0.0, 10.0));

        AnchorPane.setBottomAnchor(scrollPane, 0.0);
        AnchorPane.setLeftAnchor(scrollPane, 0.0);
        AnchorPane.setRightAnchor(scrollPane, 0.0);
        AnchorPane.setTopAnchor(scrollPane, 30.0);
        scrollPane.setLayoutX(6.0);
        scrollPane.setPrefHeight(720.0);
        scrollPane.setPrefWidth(1150.0);
        scrollPane.setStyle("-fx-background-color: #f1fced;");

        APinSP_Mat.setId("APinSP_Mat");
        APinSP_Mat.setMinHeight(720.0);
        APinSP_Mat.setMinWidth(1150.0);
        APinSP_Mat.setStyle("-fx-background-color: #f1fced;");

        AnchorPane.setBottomAnchor(VB_MainMat, 0.0);
        AnchorPane.setLeftAnchor(VB_MainMat, 0.0);
        AnchorPane.setRightAnchor(VB_MainMat, 0.0);
        AnchorPane.setTopAnchor(VB_MainMat, 0.0);
        VB_MainMat.setId("VB_MainMat");
        VB_MainMat.setLayoutX(0.0);
        VB_MainMat.setLayoutY(0.0);
        VB_MainMat.setStyle("-fx-background-color: #F1FCED;");

        lbl_DatosAlum.setId("lbl_DatosAlum");
        lbl_DatosAlum.setText("Datos del alumno:");
        lbl_DatosAlum.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        lbl_DatosAlum.setTextFill(javafx.scene.paint.Color.valueOf("#0060c0"));
        lbl_DatosAlum.setFont(new Font("Segoe UI Semilight", 24.0));
        VBox.setMargin(lbl_DatosAlum, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_NomApeCedFNAlum.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NomApeCedFNAlum.setId("HB_NomApeCedFNAlum");
        HB_NomApeCedFNAlum.setPadding(new Insets(5.0, 0.0, 0.0, 10.0));

        lbl_NombreAlum.setId("lbl_NombreAlum");
        lbl_NombreAlum.setText("Nombres:");
        lbl_NombreAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NombreAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NombreAlum.setId("tf_NombreAlum");
        tf_NombreAlum.setPromptText("Nombres");
        tf_NombreAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NombreAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NombreAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_ApeAlum.setId("lbl_ApeAlum");
        lbl_ApeAlum.setText("Apellidos:");
        lbl_ApeAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_ApeAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_ApeAlum.setId("tf_ApeAlum");
        tf_ApeAlum.setPromptText("Apellidos");
        tf_ApeAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_ApeAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_ApeAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CedAlum.setId("lbl_CedAlum");
        lbl_CedAlum.setText("Cedula:");
        lbl_CedAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CedAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_CedAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CedAlum.setId("tf_CedAlum");
        tf_CedAlum.setPromptText("Cedula");
        tf_CedAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CedAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CedAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_FNAlum.setId("lbl_FNAlum");
        lbl_FNAlum.setText("Fecha Nac.:");
        lbl_FNAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_FNAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_FNAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        dp_FNAlum.setId("dp_FNAlum");
        dp_FNAlum.setPromptText("Fecha Naciemiento");
        VBox.setMargin(HB_NomApeCedFNAlum, new Insets(5.0, 0.0, 5.0, 0.0));

        HB_NacTelCEAlum.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NacTelCEAlum.setId("HB_NacTelCEAlum");
        HB_NacTelCEAlum.setPadding(new Insets(5.0, 0.0, 0.0, 10.0));

        lbl_NacAlum.setId("lbl_NacAlum");
        lbl_NacAlum.setText("Nacionalidad:");
        lbl_NacAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NacAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NacAlum.setId("tf_NacAlum");
        tf_NacAlum.setPromptText("Nacionalidad");
        tf_NacAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NacAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NacAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_TelCasaAlum.setId("lbl_TelCasaAlum");
        lbl_TelCasaAlum.setText("Telefono casa:");
        lbl_TelCasaAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_TelCasaAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_TelCasaAlum.setId("tf_TelCasaAlum");
        tf_TelCasaAlum.setPromptText("Telefono Casa");
        tf_TelCasaAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_TelCasaAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_TelCasaAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CEAlum.setId("lbl_CEAlum");
        lbl_CEAlum.setText("Contacto Emergencia:");
        lbl_CEAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CEAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_CEAlum, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CEAlum.setId("tf_CEAlum");
        tf_CEAlum.setPromptText("Contacto de emergencia");
        tf_CEAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CEAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CEAlum, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_NacTelCEAlum, new Insets(5.0, 0.0, 5.0, 0.0));

        HB_DirAlum.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_DirAlum.setId("HB_DirAlum");
        HB_DirAlum.setPadding(new Insets(5.0, 0.0, 0.0, 10.0));

        lbl_DirAlum.setId("lbl_DirAlum");
        lbl_DirAlum.setText("Direccion completa:");
        lbl_DirAlum.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_DirAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_DirAlum.setId("tf_DirAlum");
        tf_DirAlum.setPrefHeight(27.0);
        tf_DirAlum.setPrefWidth(500.0);
        tf_DirAlum.setPromptText("Direccion");
        tf_DirAlum.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_DirAlum.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_DirAlum, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_DirAlum, new Insets(5.0, 0.0, 5.0, 0.0));

        lbl_DatosPadre.setId("lbl_DatosPadre");
        lbl_DatosPadre.setText("Datos del padre:");
        lbl_DatosPadre.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        lbl_DatosPadre.setTextFill(javafx.scene.paint.Color.valueOf("#0060c0"));
        lbl_DatosPadre.setFont(new Font("Segoe UI Semilight", 24.0));
        VBox.setMargin(lbl_DatosPadre, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_NomApeCedFNPadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NomApeCedFNPadre.setId("HB_NomApeCedFNPadre");
        HB_NomApeCedFNPadre.setPadding(new Insets(5.0, 0.0, 0.0, 10.0));

        lbl_NombrePadre.setId("lbl_NombrePadre");
        lbl_NombrePadre.setText("Nombres:");
        lbl_NombrePadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NombrePadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NombrePadre.setId("tf_NombrePadre");
        tf_NombrePadre.setPromptText("Nombres");
        tf_NombrePadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NombrePadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NombrePadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_ApePadre.setId("lbl_ApePadre");
        lbl_ApePadre.setText("Apellidos:");
        lbl_ApePadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_ApePadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_ApePadre.setId("tf_ApePadre");
        tf_ApePadre.setPromptText("Apellidos");
        tf_ApePadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_ApePadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_ApePadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CedPadre.setId("lbl_CedPadre");
        lbl_CedPadre.setText("Cedula:");
        lbl_CedPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CedPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_CedPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CedPadre.setId("tf_CedPadre");
        tf_CedPadre.setPromptText("Cedula");
        tf_CedPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CedPadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CedPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_FNPadre.setId("lbl_FNPadre");
        lbl_FNPadre.setText("Fecha Nac.:");
        lbl_FNPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_FNPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_FNPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        dp_FNPadre.setId("dp_FNPadre");
        dp_FNPadre.setPromptText("Fecha Naciemiento");
        VBox.setMargin(HB_NomApeCedFNPadre, new Insets(5.0, 0.0, 5.0, 0.0));

        HB_NacCelNEPadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NacCelNEPadre.setId("HB_NacCelNEPadre");
        HB_NacCelNEPadre.setPadding(new Insets(0.0, 0.0, 0.0, 10.0));

        lbl_NacPadre.setId("lbl_NacPadre");
        lbl_NacPadre.setText("Nacionalidad:");
        lbl_NacPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NacPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NacPadre.setId("tf_NacPadre");
        tf_NacPadre.setPromptText("Nacionalidad");
        tf_NacPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NacPadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NacPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CelPadre.setId("lbl_CelPadre");
        lbl_CelPadre.setText("Celular:");
        lbl_CelPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CelPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CelPadre.setId("tf_CelPadre");
        tf_CelPadre.setPromptText("Celular");
        tf_CelPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CelPadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CelPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_NEPadre.setId("lbl_NEPadre");
        lbl_NEPadre.setText("Nivel de educacion:");
        lbl_NEPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NEPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_NEPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NEPadre.setId("tf_NEPadre");
        tf_NEPadre.setPromptText("Nivel");
        tf_NEPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NEPadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NEPadre, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_NacCelNEPadre, new Insets(5.0, 0.0, 5.0, 0.0));

        HB_ApRECVcEPadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_ApRECVcEPadre.setId("HB_ApRECVcEPadre");
        HB_ApRECVcEPadre.setPadding(new Insets(0.0, 0.0, 0.0, 10.0));

        lbl_ApRPadre.setId("lbl_ApRPadre");
        lbl_ApRPadre.setText("Autorizado para retirar:");
        lbl_ApRPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_ApRPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        cb_ApRPadre.setId("cb_ApRPadre");
        cb_ApRPadre.setPrefWidth(50.0);

        lbl_ECPadre.setId("lbl_ECPadre");
        lbl_ECPadre.setText("Estado civil:");
        lbl_ECPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_ECPadre, new Insets(0.0, 5.0, 0.0, 5.0));

        cb_ECPadre.setId("cb_ECPadre");
        cb_ECPadre.setPrefWidth(150.0);

        lbl_VcEPadre.setId("lbl_VcEPadre");
        lbl_VcEPadre.setText("Vive con estudiante:");
        lbl_VcEPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_VcEPadre, new Insets(0.0, 5.0, 0.0, 5.0));

        cb_VcEPadre.setId("cb_VcEPadre");
        cb_VcEPadre.setPrefWidth(50.0);
        VBox.setMargin(HB_ApRECVcEPadre, new Insets(5.0, 0.0, 5.0, 0.0));

        HB_EmailOcuPadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_EmailOcuPadre.setId("HB_EmailOcuPadre");

        lbl_EmailPadre.setId("lbl_EmailPadre");
        lbl_EmailPadre.setText("Email:");
        lbl_EmailPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_EmailPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_EmailPadre.setId("tf_EmailPadre");
        tf_EmailPadre.setPrefHeight(27.0);
        tf_EmailPadre.setPrefWidth(500.0);
        tf_EmailPadre.setPromptText("Direccion de correo electronico");
        tf_EmailPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_EmailPadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_EmailPadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_OcupacionPadre.setId("lbl_OcupacionPadre");
        lbl_OcupacionPadre.setText("Ocupacion:");
        lbl_OcupacionPadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_OcupacionPadre, new Insets(0.0, 5.0, 0.0, 5.0));

        tf_OcupacionPadre.setId("tf_OcupacionPadre");
        tf_OcupacionPadre.setPromptText("Ocupacion");
        tf_OcupacionPadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_OcupacionPadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_OcupacionPadre, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_EmailOcuPadre, new Insets(5.0, 0.0, 5.0, 10.0));

        lbl_DatosMadre.setId("lbl_DatosMadre");
        lbl_DatosMadre.setText("Datos de la madre:");
        lbl_DatosMadre.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        lbl_DatosMadre.setTextFill(javafx.scene.paint.Color.valueOf("#0060c0"));
        lbl_DatosMadre.setFont(new Font("Segoe UI Semilight", 24.0));
        VBox.setMargin(lbl_DatosMadre, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_NomApeCedFNMadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NomApeCedFNMadre.setId("HB_NomApeCedFNMadre");

        lbl_NombreMadre.setId("lbl_NombreMadre");
        lbl_NombreMadre.setText("Nombres:");
        lbl_NombreMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NombreMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NombreMadre.setId("tf_NombreMadre");
        tf_NombreMadre.setPromptText("Nombres");
        tf_NombreMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NombreMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NombreMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_ApeMadre.setId("lbl_ApeMadre");
        lbl_ApeMadre.setText("Apellidos:");
        lbl_ApeMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_ApeMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_ApeMadre.setId("tf_ApeMadre");
        tf_ApeMadre.setPromptText("Apellidos");
        tf_ApeMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_ApeMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_ApeMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CedMadre.setId("lbl_CedMadre");
        lbl_CedMadre.setText("Cedula:");
        lbl_CedMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CedMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_CedMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CedMadre.setId("tf_CedMadre");
        tf_CedMadre.setPromptText("Cedula");
        tf_CedMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CedMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CedMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_FNMadre.setId("lbl_FNMadre");
        lbl_FNMadre.setText("Fecha Nac.:");
        lbl_FNMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_FNMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_FNMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        dp_FNMadre.setId("dp_FNMadre");
        dp_FNMadre.setPromptText("Fecha Naciemiento");
        VBox.setMargin(HB_NomApeCedFNMadre, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_NacCelNEMadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NacCelNEMadre.setId("HB_NacCelNEMadre");

        lbl_NacMadre.setId("lbl_NacMadre");
        lbl_NacMadre.setText("Nacionalidad:");
        lbl_NacMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NacMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NacMadre.setId("tf_NacMadre");
        tf_NacMadre.setPromptText("Nacionalidad");
        tf_NacMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NacMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NacMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CelMadre.setId("lbl_CelMadre");
        lbl_CelMadre.setText("Celular:");
        lbl_CelMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CelMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CelMadre.setId("tf_CelMadre");
        tf_CelMadre.setPromptText("Celular");
        tf_CelMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CelMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CelMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_NEMadre.setId("lbl_NEMadre");
        lbl_NEMadre.setText("Nivel de educacion:");
        lbl_NEMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NEMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_NEMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NEMadre.setId("tf_NEMadre");
        tf_NEMadre.setPromptText("Nivel");
        tf_NEMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NEMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NEMadre, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_NacCelNEMadre, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_ApRECVcEMadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_ApRECVcEMadre.setId("HB_ApRECVcEMadre");

        lbl_ApRMadre.setId("lbl_ApRMadre");
        lbl_ApRMadre.setText("Autorizado para retirar:");
        lbl_ApRMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_ApRMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        cb_ApRMadre.setId("cb_ApRMadre");
        cb_ApRMadre.setPrefWidth(50.0);

        lbl_ECMadre.setId("lbl_ECMadre");
        lbl_ECMadre.setText("Estado civil:");
        lbl_ECMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_ECMadre, new Insets(0.0, 5.0, 0.0, 5.0));

        cb_ECMadre.setId("cb_ECMadre");
        cb_ECMadre.setPrefWidth(150.0);

        lbl_VcEMadre.setId("lbl_VcEMadre");
        lbl_VcEMadre.setText("Vive con estudiante:");
        lbl_VcEMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_VcEMadre, new Insets(0.0, 5.0, 0.0, 5.0));

        cb_VcEMadre.setId("cb_VcEMadre");
        cb_VcEMadre.setPrefWidth(50.0);
        VBox.setMargin(HB_ApRECVcEMadre, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_EmailOcuMadre.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_EmailOcuMadre.setId("HB_EmailOcuMadre");

        lbl_EmailMadre.setId("lbl_EmailMadre");
        lbl_EmailMadre.setText("Email:");
        lbl_EmailMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_EmailMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_EmailMadre.setId("tf_EmailMadre");
        tf_EmailMadre.setPrefHeight(27.0);
        tf_EmailMadre.setPrefWidth(500.0);
        tf_EmailMadre.setPromptText("Direccion de correo electronico");
        tf_EmailMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_EmailMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_EmailMadre, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_OcupacionMadre.setId("lbl_OcupacionMadre");
        lbl_OcupacionMadre.setText("Ocupacion:");
        lbl_OcupacionMadre.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_OcupacionMadre, new Insets(0.0, 5.0, 0.0, 5.0));

        tf_OcupacionMadre.setId("tf_OcupacionMadre");
        tf_OcupacionMadre.setPromptText("Ocupacion");
        tf_OcupacionMadre.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_OcupacionMadre.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_OcupacionMadre, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_EmailOcuMadre, new Insets(5.0, 0.0, 5.0, 10.0));

        lbl_DatosRep.setId("lbl_DatosRep");
        lbl_DatosRep.setText("Representante del estudiante:");
        lbl_DatosRep.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        lbl_DatosRep.setTextFill(javafx.scene.paint.Color.valueOf("#0060c0"));
        lbl_DatosRep.setFont(new Font("Segoe UI Semilight", 24.0));
        VBox.setMargin(lbl_DatosRep, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_NomApeCedFNRep.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NomApeCedFNRep.setId("HB_NomApeCedFNRep");

        lbl_NombreRep.setId("lbl_NombreRep");
        lbl_NombreRep.setText("Nombres:");
        lbl_NombreRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NombreRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NombreRep.setId("tf_NombreRep");
        tf_NombreRep.setPromptText("Nombres");
        tf_NombreRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NombreRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NombreRep, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_ApeRep.setId("lbl_ApeRep");
        lbl_ApeRep.setText("Apellidos:");
        lbl_ApeRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_ApeRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_ApeRep.setId("tf_ApeRep");
        tf_ApeRep.setPromptText("Apellidos");
        tf_ApeRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_ApeRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_ApeRep, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CedRep.setId("lbl_CedRep");
        lbl_CedRep.setText("Cedula:");
        lbl_CedRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CedRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_CedRep, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CedRep.setId("tf_CedRep");
        tf_CedRep.setPromptText("Cedula");
        tf_CedRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CedRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CedRep, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_FNRep.setId("lbl_FNRep");
        lbl_FNRep.setText("Fecha Nac.:");
        lbl_FNRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_FNRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_FNRep, new Insets(0.0, 5.0, 0.0, 0.0));

        dp_FNRep.setId("dp_FNRep");
        dp_FNRep.setPromptText("Fecha Naciemiento");
        VBox.setMargin(HB_NomApeCedFNRep, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_NacCelNERep.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_NacCelNERep.setId("HB_NacCelNERep");

        lbl_NacRep.setId("lbl_NacRep");
        lbl_NacRep.setText("Nacionalidad:");
        lbl_NacRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NacRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NacRep.setId("tf_NacRep");
        tf_NacRep.setPromptText("Nacionalidad");
        tf_NacRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NacRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NacRep, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_CelRep.setId("lbl_CelRep");
        lbl_CelRep.setText("Celular:");
        lbl_CelRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_CelRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_CelRep.setId("tf_CelRep");
        tf_CelRep.setPromptText("Celular");
        tf_CelRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_CelRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_CelRep, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_NERep.setId("lbl_NERep");
        lbl_NERep.setText("Nivel de educacion:");
        lbl_NERep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_NERep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        HBox.setMargin(lbl_NERep, new Insets(0.0, 5.0, 0.0, 0.0));

        tf_NERep.setId("tf_NERep");
        tf_NERep.setPromptText("Nivel");
        tf_NERep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_NERep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_NERep, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_NacCelNERep, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_ApRECVcERep.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_ApRECVcERep.setId("HB_ApRECVcERep");

        lbl_ApRRep.setId("lbl_ApRRep");
        lbl_ApRRep.setText("Autorizado para retirar:");
        lbl_ApRRep.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_ApRRep, new Insets(0.0, 5.0, 0.0, 0.0));

        cb_ApRRep.setId("cb_ApRRep");
        cb_ApRRep.setPrefWidth(50.0);

        lbl_ECRep.setId("lbl_ECRep");
        lbl_ECRep.setText("Estado civil:");
        lbl_ECRep.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_ECRep, new Insets(0.0, 5.0, 0.0, 5.0));

        cb_ECRep.setId("cb_ECRep");
        cb_ECRep.setPrefWidth(150.0);

        lbl_VcERep.setId("lbl_VcERep");
        lbl_VcERep.setText("Vive con estudiante:");
        lbl_VcERep.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_VcERep, new Insets(0.0, 5.0, 0.0, 5.0));

        cb_VcERep.setId("cb_VcERep");
        cb_VcERep.setPrefWidth(50.0);
        VBox.setMargin(HB_ApRECVcERep, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_EmailOcuRep.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        HB_EmailOcuRep.setId("HB_EmailOcuRep");

        lbl_EmailRep.setId("lbl_EmailRep");
        lbl_EmailRep.setText("Email:");
        lbl_EmailRep.setFont(new Font("Segoe UI Semilight", 18.0));
        lbl_EmailRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));

        tf_EmailRep.setId("tf_EmailRep");
        tf_EmailRep.setPrefHeight(27.0);
        tf_EmailRep.setPrefWidth(500.0);
        tf_EmailRep.setPromptText("Direccion de correo electronico");
        tf_EmailRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_EmailRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_EmailRep, new Insets(0.0, 5.0, 0.0, 0.0));

        lbl_OcupacionRep.setId("lbl_OcupacionRep");
        lbl_OcupacionRep.setText("Ocupacion:");
        lbl_OcupacionRep.setFont(new Font("Segoe UI Semilight", 18.0));
        HBox.setMargin(lbl_OcupacionRep, new Insets(0.0, 5.0, 0.0, 5.0));

        tf_OcupacionRep.setId("tf_OcupacionRep");
        tf_OcupacionRep.setPromptText("Ocupacion");
        tf_OcupacionRep.setPadding(new Insets(0.0, 5.0, 0.0, 0.0));
        tf_OcupacionRep.setFont(new Font("Segoe UI Light", 18.0));
        HBox.setMargin(tf_OcupacionRep, new Insets(0.0, 5.0, 0.0, 0.0));
        VBox.setMargin(HB_EmailOcuRep, new Insets(5.0, 0.0, 5.0, 10.0));

        HB_Botones.setAlignment(javafx.geometry.Pos.CENTER);
        HB_Botones.setId("HB_Botones");
        HB_Botones.setPrefHeight(100.0);
        HB_Botones.setPrefWidth(200.0);

        btn_Matricular.setId("btn_Matricular");
        btn_Matricular.setMnemonicParsing(false);
        btn_Matricular.setText("Matricular");
        HBox.setMargin(btn_Matricular, new Insets(5.0, 50.0, 5.0, 0.0));

        btn_Cancelar.setId("btn_Cancelar");
        btn_Cancelar.setMnemonicParsing(false);
        btn_Cancelar.setText("Cancelar");
        HBox.setMargin(btn_Cancelar, new Insets(5.0, 0.0, 5.0, 0.0));
    }
    
    private void poblar(){
        this.getChildren().add(lbl_Errores);
        VB_MainMat.getChildren().add(lbl_DatosAlum);
        HB_NomApeCedFNAlum.getChildren().add(lbl_NombreAlum);
        HB_NomApeCedFNAlum.getChildren().add(tf_NombreAlum);
        HB_NomApeCedFNAlum.getChildren().add(lbl_ApeAlum);
        HB_NomApeCedFNAlum.getChildren().add(tf_ApeAlum);
        HB_NomApeCedFNAlum.getChildren().add(lbl_CedAlum);
        HB_NomApeCedFNAlum.getChildren().add(tf_CedAlum);
        HB_NomApeCedFNAlum.getChildren().add(lbl_FNAlum);
        HB_NomApeCedFNAlum.getChildren().add(dp_FNAlum);
        VB_MainMat.getChildren().add(HB_NomApeCedFNAlum);
        HB_NacTelCEAlum.getChildren().add(lbl_NacAlum);
        HB_NacTelCEAlum.getChildren().add(tf_NacAlum);
        HB_NacTelCEAlum.getChildren().add(lbl_TelCasaAlum);
        HB_NacTelCEAlum.getChildren().add(tf_TelCasaAlum);
        HB_NacTelCEAlum.getChildren().add(lbl_CEAlum);
        HB_NacTelCEAlum.getChildren().add(tf_CEAlum);
        VB_MainMat.getChildren().add(HB_NacTelCEAlum);
        HB_DirAlum.getChildren().add(lbl_DirAlum);
        HB_DirAlum.getChildren().add(tf_DirAlum);
        VB_MainMat.getChildren().add(HB_DirAlum);
        VB_MainMat.getChildren().add(lbl_DatosPadre);
        HB_NomApeCedFNPadre.getChildren().add(lbl_NombrePadre);
        HB_NomApeCedFNPadre.getChildren().add(tf_NombrePadre);
        HB_NomApeCedFNPadre.getChildren().add(lbl_ApePadre);
        HB_NomApeCedFNPadre.getChildren().add(tf_ApePadre);
        HB_NomApeCedFNPadre.getChildren().add(lbl_CedPadre);
        HB_NomApeCedFNPadre.getChildren().add(tf_CedPadre);
        HB_NomApeCedFNPadre.getChildren().add(lbl_FNPadre);
        HB_NomApeCedFNPadre.getChildren().add(dp_FNPadre);
        VB_MainMat.getChildren().add(HB_NomApeCedFNPadre);
        HB_NacCelNEPadre.getChildren().add(lbl_NacPadre);
        HB_NacCelNEPadre.getChildren().add(tf_NacPadre);
        HB_NacCelNEPadre.getChildren().add(lbl_CelPadre);
        HB_NacCelNEPadre.getChildren().add(tf_CelPadre);
        HB_NacCelNEPadre.getChildren().add(lbl_NEPadre);
        HB_NacCelNEPadre.getChildren().add(tf_NEPadre);
        VB_MainMat.getChildren().add(HB_NacCelNEPadre);
        HB_ApRECVcEPadre.getChildren().add(lbl_ApRPadre);
        HB_ApRECVcEPadre.getChildren().add(cb_ApRPadre);
        HB_ApRECVcEPadre.getChildren().add(lbl_ECPadre);
        HB_ApRECVcEPadre.getChildren().add(cb_ECPadre);
        HB_ApRECVcEPadre.getChildren().add(lbl_VcEPadre);
        HB_ApRECVcEPadre.getChildren().add(cb_VcEPadre);
        VB_MainMat.getChildren().add(HB_ApRECVcEPadre);
        HB_EmailOcuPadre.getChildren().add(lbl_EmailPadre);
        HB_EmailOcuPadre.getChildren().add(tf_EmailPadre);
        HB_EmailOcuPadre.getChildren().add(lbl_OcupacionPadre);
        HB_EmailOcuPadre.getChildren().add(tf_OcupacionPadre);
        VB_MainMat.getChildren().add(HB_EmailOcuPadre);
        VB_MainMat.getChildren().add(lbl_DatosMadre);
        HB_NomApeCedFNMadre.getChildren().add(lbl_NombreMadre);
        HB_NomApeCedFNMadre.getChildren().add(tf_NombreMadre);
        HB_NomApeCedFNMadre.getChildren().add(lbl_ApeMadre);
        HB_NomApeCedFNMadre.getChildren().add(tf_ApeMadre);
        HB_NomApeCedFNMadre.getChildren().add(lbl_CedMadre);
        HB_NomApeCedFNMadre.getChildren().add(tf_CedMadre);
        HB_NomApeCedFNMadre.getChildren().add(lbl_FNMadre);
        HB_NomApeCedFNMadre.getChildren().add(dp_FNMadre);
        VB_MainMat.getChildren().add(HB_NomApeCedFNMadre);
        HB_NacCelNEMadre.getChildren().add(lbl_NacMadre);
        HB_NacCelNEMadre.getChildren().add(tf_NacMadre);
        HB_NacCelNEMadre.getChildren().add(lbl_CelMadre);
        HB_NacCelNEMadre.getChildren().add(tf_CelMadre);
        HB_NacCelNEMadre.getChildren().add(lbl_NEMadre);
        HB_NacCelNEMadre.getChildren().add(tf_NEMadre);
        VB_MainMat.getChildren().add(HB_NacCelNEMadre);
        HB_ApRECVcEMadre.getChildren().add(lbl_ApRMadre);
        HB_ApRECVcEMadre.getChildren().add(cb_ApRMadre);
        HB_ApRECVcEMadre.getChildren().add(lbl_ECMadre);
        HB_ApRECVcEMadre.getChildren().add(cb_ECMadre);
        HB_ApRECVcEMadre.getChildren().add(lbl_VcEMadre);
        HB_ApRECVcEMadre.getChildren().add(cb_VcEMadre);
        VB_MainMat.getChildren().add(HB_ApRECVcEMadre);
        HB_EmailOcuMadre.getChildren().add(lbl_EmailMadre);
        HB_EmailOcuMadre.getChildren().add(tf_EmailMadre);
        HB_EmailOcuMadre.getChildren().add(lbl_OcupacionMadre);
        HB_EmailOcuMadre.getChildren().add(tf_OcupacionMadre);
        VB_MainMat.getChildren().add(HB_EmailOcuMadre);
        VB_MainMat.getChildren().add(lbl_DatosRep);
        HB_NomApeCedFNRep.getChildren().add(lbl_NombreRep);
        HB_NomApeCedFNRep.getChildren().add(tf_NombreRep);
        HB_NomApeCedFNRep.getChildren().add(lbl_ApeRep);
        HB_NomApeCedFNRep.getChildren().add(tf_ApeRep);
        HB_NomApeCedFNRep.getChildren().add(lbl_CedRep);
        HB_NomApeCedFNRep.getChildren().add(tf_CedRep);
        HB_NomApeCedFNRep.getChildren().add(lbl_FNRep);
        HB_NomApeCedFNRep.getChildren().add(dp_FNRep);
        VB_MainMat.getChildren().add(HB_NomApeCedFNRep);
        HB_NacCelNERep.getChildren().add(lbl_NacRep);
        HB_NacCelNERep.getChildren().add(tf_NacRep);
        HB_NacCelNERep.getChildren().add(lbl_CelRep);
        HB_NacCelNERep.getChildren().add(tf_CelRep);
        HB_NacCelNERep.getChildren().add(lbl_NERep);
        HB_NacCelNERep.getChildren().add(tf_NERep);
        VB_MainMat.getChildren().add(HB_NacCelNERep);
        HB_ApRECVcERep.getChildren().add(lbl_ApRRep);
        HB_ApRECVcERep.getChildren().add(cb_ApRRep);
        HB_ApRECVcERep.getChildren().add(lbl_ECRep);
        HB_ApRECVcERep.getChildren().add(cb_ECRep);
        HB_ApRECVcERep.getChildren().add(lbl_VcERep);
        HB_ApRECVcERep.getChildren().add(cb_VcERep);
        VB_MainMat.getChildren().add(HB_ApRECVcERep);
        HB_EmailOcuRep.getChildren().add(lbl_EmailRep);
        HB_EmailOcuRep.getChildren().add(tf_EmailRep);
        HB_EmailOcuRep.getChildren().add(lbl_OcupacionRep);
        HB_EmailOcuRep.getChildren().add(tf_OcupacionRep);
        VB_MainMat.getChildren().add(HB_EmailOcuRep);
        HB_Botones.getChildren().add(btn_Matricular);
        HB_Botones.getChildren().add(btn_Cancelar);
        VB_MainMat.getChildren().add(HB_Botones);
        APinSP_Mat.getChildren().add(VB_MainMat);
        scrollPane.setContent(APinSP_Mat);
        this.getChildren().add(scrollPane);
    }
    
    private void asigBtns(){
        asigMat();
        asigCan();
    }
    
    private boolean esCedula(TextField tf){
        String ced = tf.getText();
        return ced.length() == 10 && ced.matches("^[0-9]+$");
    }
    
    private boolean esTel(TextField tf){
        String ced = tf.getText();
        return (ced.length() == 10 || ced.length() == 7 || ced.length() == 9) 
                && ced.matches("^[0-9]+$");
    }
    
    private boolean checkTelefonos(){
        boolean ret = esTel(this.tf_TelCasaAlum) && esTel(this.tf_CelPadre) 
                && esTel(this.tf_CelMadre) && esTel(this.tf_CelRep);
        if (!ret){
            System.out.println("Telefonos");
        }
        return ret;
    }
    
    private boolean checkCedulas(){
        return esCedula(this.tf_CedAlum) && esCedula(this.tf_CedPadre)
                && esCedula(this.tf_CedMadre) && esCedula(this.tf_CedRep);
    }
    
    private void asigMat(){
        btn_Matricular.setOnAction((ActionEvent event) -> {
            if (checkAllTextField() && checkAllDate() && checkAllChoiceBox()
                    && checkCedulas() && checkTelefonos()){
                this.lbl_Errores.setText("");
                cdb.matricular(this.tf_NombreAlum.getText(),
                        this.tf_ApeAlum.getText(),
                        this.tf_CedAlum.getText(),
                        this.dp_FNAlum.getValue(),
                        this.tf_NacAlum.getText(),
                        this.tf_TelCasaAlum.getText(),
                        this.tf_CEAlum.getText(),
                        this.tf_DirAlum.getText(),
                        this.tf_NombrePadre.getText(),
                        this.tf_ApePadre.getText(),
                        this.tf_CedPadre.getText(),
                        this.dp_FNPadre.getValue(),
                        this.tf_NacPadre.getText(),
                        this.tf_CelPadre.getText(),
                        this.tf_NEPadre.getText(),
                        getAut(this.cb_ApRPadre),
                        this.cb_ECPadre.getSelectionModel().getSelectedItem()
                                .toString(),
                        this.cb_VcEPadre.getSelectionModel().getSelectedItem()
                                .toString().equals("Vive"),
                        this.tf_EmailPadre.getText(),
                        this.tf_OcupacionPadre.getText(),
                        this.tf_NombreMadre.getText(),
                        this.tf_ApeMadre.getText(),
                        this.tf_CedMadre.getText(),
                        this.dp_FNMadre.getValue(),
                        this.tf_NacMadre.getText(),
                        this.tf_CelMadre.getText(),
                        this.tf_NEMadre.getText(),
                        getAut(this.cb_ApRMadre),
                        this.cb_ECMadre.getSelectionModel().getSelectedItem()
                                .toString(),
                        this.cb_VcEMadre.getSelectionModel().getSelectedItem()
                                .toString().equals("Vive"),
                        this.tf_EmailMadre.getText(),
                        this.tf_OcupacionMadre.getText(),
                        this.tf_NombreRep.getText(),
                        this.tf_ApeRep.getText(),
                        this.tf_CedRep.getText(),
                        this.dp_FNRep.getValue(),
                        this.tf_NacRep.getText(),
                        this.tf_CelRep.getText(),
                        this.tf_NERep.getText(),
                        getAut(this.cb_ApRRep),
                        this.cb_ECRep.getSelectionModel().getSelectedItem()
                                .toString(),
                        this.cb_VcERep.getSelectionModel().getSelectedItem()
                                .toString().equals("Vive"),
                        this.tf_EmailRep.getText(),
                        this.tf_OcupacionRep.getText());
                getAllTextField().forEach((tf) -> {
                    tf.setText("");
                 });
            } else {
                this.lbl_Errores.setText("Valores Incorrectos");
            }
        });
    }
    
    private boolean getAut(ChoiceBox cb){
        String choice = cb.getSelectionModel().getSelectedItem()
                .toString();
        return choice.equals("Autorizado");
    }
    
    private boolean checkAllTextField(){
        boolean allComplete = true;
        for (TextField tf : getAllTextField()) {
            allComplete = allComplete && !tf.getText().equals("");
            if (!allComplete){
                return false;
            }
        }
        return allComplete;
    }
    
    private ArrayList<TextField> getAllTextField(){
        ArrayList<TextField> ret = new ArrayList<>();
        ret.add(tf_NombreAlum);
        ret.add(tf_ApeAlum);
        ret.add(tf_CedAlum);
        ret.add(tf_NacAlum);
        ret.add(tf_TelCasaAlum);
        ret.add(tf_CEAlum);
        ret.add(tf_DirAlum);
        ret.add(tf_NombrePadre);
        ret.add(tf_ApePadre);
        ret.add(tf_CedPadre);
        ret.add(tf_NacPadre);
        ret.add(tf_CelPadre);
        ret.add(tf_NEPadre);
        ret.add(tf_EmailPadre);
        ret.add(tf_OcupacionPadre);
        ret.add(tf_NombreMadre);
        ret.add(tf_ApeMadre);
        ret.add(tf_CedMadre);
        ret.add(tf_NacMadre);
        ret.add(tf_CelMadre);
        ret.add(tf_NEMadre);
        ret.add(tf_EmailMadre);
        ret.add(tf_OcupacionMadre);
        ret.add(tf_NombreRep);
        ret.add(tf_ApeRep);
        ret.add(tf_CedRep);
        ret.add(tf_NacRep);
        ret.add(tf_CelRep);
        ret.add(tf_NERep);
        ret.add(tf_EmailRep);
        ret.add(tf_OcupacionRep);
        
        return ret;
    }
    
    private boolean checkAllDate(){
        boolean allComplete = true;
        for (DatePicker dp : getAllDate()) {
            allComplete = allComplete && dp.getValue() != null;
            if (!allComplete){
                return false;
            }
        }
        return allComplete;
    }
    
    private ArrayList<DatePicker> getAllDate(){
        ArrayList<DatePicker> ret = new ArrayList<>();
        ret.add(dp_FNAlum);
        ret.add(dp_FNPadre);
        ret.add(dp_FNMadre);
        ret.add(dp_FNRep);
        
        return ret;
    }
    
    private boolean checkAllChoiceBox(){
        boolean allComplete = true;
        for (ChoiceBox cb : getAllCB()) {
            allComplete = allComplete && !cb.getSelectionModel().isEmpty();
            if (!allComplete){
                return false;
            }
        }
        return allComplete;
    }
    
    private ArrayList<ChoiceBox> getAllCB(){
        ArrayList<ChoiceBox> ret = new ArrayList<>();
        ret.add(cb_ApRPadre);
        ret.add(cb_ECPadre);
        ret.add(cb_VcEPadre);
        ret.add(cb_ApRMadre);
        ret.add(cb_ECMadre);
        ret.add(cb_VcEMadre);
        ret.add(cb_ApRRep);
        ret.add(cb_ECRep);
        ret.add(cb_VcERep);
        
        return ret;
    }
    
    private void asigCan(){
        btn_Cancelar.setOnAction((ActionEvent event) -> {
            getAllTextField().forEach((tf) -> {
                tf.setText("");
            });
        });
    }
}
