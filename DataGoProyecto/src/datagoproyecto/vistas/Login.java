package datagoproyecto.vistas;

import datagoproyecto.MainUI;
import datagoproyecto.conexion.Anschluss;
import java.sql.ResultSet;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class Login extends AnchorPane {

    protected final HBox hb_Mundo;
    protected final Label lbd_Mundo;
    protected final VBox vb_control;
    protected final HBox hb_user;
    protected final Label lbl_user;
    protected final TextField txt_usuario;
    protected final HBox hb_contra;
    protected final Label lbl_contra;
    protected final PasswordField txt_contra;
    protected final HBox hb_iniciar;
    protected final Button btn_iniciar;
    private MainUI main;

    public Login(MainUI main) {
        
        this.main = main;

        hb_Mundo = new HBox();
        lbd_Mundo = new Label();
        vb_control = new VBox();
        hb_user = new HBox();
        lbl_user = new Label();
        txt_usuario = new TextField();
        hb_contra = new HBox();
        lbl_contra = new Label();
        txt_contra = new PasswordField();
        hb_iniciar = new HBox();
        btn_iniciar = new Button();

        setId("AnchorPane");
        setPrefHeight(720.0);
        setPrefWidth(1280.0);
        setStyle("-fx-background-color: #f1fced; -fx-background-image: ;");

        hb_Mundo.setId("hb_Mundo");
        hb_Mundo.setLayoutX(365.0);
        hb_Mundo.setLayoutY(119.0);
        hb_Mundo.setPrefHeight(104.0);
        hb_Mundo.setPrefWidth(489.0);

        lbd_Mundo.setContentDisplay(javafx.scene.control.ContentDisplay.CENTER);
        lbd_Mundo.setId("lbd_Mundo");
        lbd_Mundo.setPrefHeight(101.0);
        lbd_Mundo.setPrefWidth(484.0);
        lbd_Mundo.setText("CEI Mundo de Colores");
        lbd_Mundo.setTextFill(javafx.scene.paint.Color.valueOf("#252424"));
        lbd_Mundo.setFont(new Font(43.0));

        vb_control.setId("vb_control");
        vb_control.setLayoutX(375.0);
        vb_control.setLayoutY(256.0);
        vb_control.setPrefHeight(352.0);
        vb_control.setPrefWidth(444.0);

        hb_user.setId("hb_user");
        hb_user.setPrefHeight(100.0);
        hb_user.setPrefWidth(200.0);

        lbl_user.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_user.setId("lbl_user");
        lbl_user.setPrefHeight(97.0);
        lbl_user.setPrefWidth(167.0);
        lbl_user.setText("Usuario");
        lbl_user.setTextAlignment(javafx.scene.text.TextAlignment.CENTER);
        lbl_user.setFont(new Font(33.0));

        txt_usuario.setAlignment(javafx.geometry.Pos.CENTER_LEFT);
        txt_usuario.setId("txt_usuario");
        txt_usuario.setPrefHeight(86.0);
        txt_usuario.setPrefWidth(258.0);
        HBox.setMargin(txt_usuario, new Insets(0.0));
        txt_usuario.setFont(new Font(26.0));

        hb_contra.setId("hb_contra");
        hb_contra.setPrefHeight(100.0);
        hb_contra.setPrefWidth(200.0);

        lbl_contra.setId("lbl_contra");
        lbl_contra.setPrefHeight(104.0);
        lbl_contra.setPrefWidth(168.0);
        lbl_contra.setText("Contraseña");
        lbl_contra.setFont(new Font(32.0));

        txt_contra.setId("txt_contra");
        txt_contra.setPrefHeight(97.0);
        txt_contra.setPrefWidth(260.0);

        VBox.setVgrow(hb_iniciar, javafx.scene.layout.Priority.ALWAYS);
        hb_iniciar.setAlignment(javafx.geometry.Pos.CENTER);
        hb_iniciar.setId("hb_iniciar");
        hb_iniciar.setPrefHeight(100.0);
        hb_iniciar.setPrefWidth(200.0);

        btn_iniciar.setId("btn_iniciar");
        btn_iniciar.setMnemonicParsing(false);
        btn_iniciar.setPrefHeight(112.0);
        btn_iniciar.setPrefWidth(200.0);
        btn_iniciar.setText("Iniciar Sesion");
        btn_iniciar.setFont(new Font(26.0));

        hb_Mundo.getChildren().add(lbd_Mundo);
        getChildren().add(hb_Mundo);
        hb_user.getChildren().add(lbl_user);
        hb_user.getChildren().add(txt_usuario);
        vb_control.getChildren().add(hb_user);
        hb_contra.getChildren().add(lbl_contra);
        hb_contra.getChildren().add(txt_contra);
        vb_control.getChildren().add(hb_contra);
        hb_iniciar.getChildren().add(btn_iniciar);
        vb_control.getChildren().add(hb_iniciar);
        getChildren().add(vb_control);

        asigBtn();
    }
    
    private void asigBtn(){
        btn_iniciar.setOnAction((ActionEvent event) -> {
            checkUNPW();
        });
    }
    
    private void changeUI(Anschluss conexion){
        main.changeUI(new ControlPanel(conexion, main));
    }
    
    private void checkUNPW(){
        if (!txt_usuario.getText().equals("") && !txt_contra.getText().equals("")){
            try{
                Anschluss cdb = new Anschluss(txt_usuario.getText().replace(";", "\\;"), txt_contra.getText().replace(";", "\\;"));
                if (cdb.isConnected()){
                    changeUI(cdb);
                } else {
                    System.out.println("Usuario no existe o contraseña incorrecta");
                }
            }catch(Exception e){
                System.out.println(e);
            }
        }
    }
}
