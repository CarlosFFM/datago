package datagoproyecto.vistas;

import datagoproyecto.conexion.Anschluss;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

public class displayPagos extends HBox {
    
    private Anschluss cdb;

    protected final Label lbl_Nombre;
    protected final Label lbl_Apellido;
    protected final Label lbl_Fecha;
    protected final Label lbl_Valor;
    protected final Label lbl_Estado;
    protected final Label lbl_Descripcion;
    protected final Button btn_Pagar;

    public displayPagos(Anschluss cdb) {
        
        this.cdb = cdb;

        lbl_Nombre = new Label();
        lbl_Apellido = new Label();
        lbl_Fecha = new Label();
        lbl_Valor = new Label();
        lbl_Estado = new Label();
        lbl_Descripcion = new Label();
        btn_Pagar = new Button();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));

        lbl_Nombre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Nombre.setId("lbl_Nombre");
        lbl_Nombre.setPrefWidth(200.0);
        lbl_Nombre.setText("Nombre");
        lbl_Nombre.setFont(new Font("Segoe UI Semilight", 16.0));
        HBox.setMargin(lbl_Nombre, new Insets(0.0, 0.0, 0.0, 5.0));

        lbl_Apellido.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Apellido.setId("lbl_Apellido");
        lbl_Apellido.setPrefWidth(200.0);
        lbl_Apellido.setText("Apellido");
        lbl_Apellido.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Fecha.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setLayoutX(215.0);
        lbl_Fecha.setLayoutY(15.0);
        lbl_Fecha.setPrefWidth(100.0);
        lbl_Fecha.setText("Mes - Anio");
        lbl_Fecha.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Valor.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Valor.setId("lbl_Valor");
        lbl_Valor.setPrefWidth(75.0);
        lbl_Valor.setText("Valor");
        lbl_Valor.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Estado.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Estado.setId("lbl_Estado");
        lbl_Estado.setPrefWidth(125.0);
        lbl_Estado.setText("Estado");
        lbl_Estado.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Descripcion.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Descripcion.setId("lbl_Descripcion");
        lbl_Descripcion.setPrefWidth(350.0);
        lbl_Descripcion.setText("Descripcion");
        lbl_Descripcion.setFont(new Font("Segoe UI Semilight", 16.0));

        btn_Pagar.setId("btn_Pagar");
        btn_Pagar.setMnemonicParsing(false);
        btn_Pagar.setPrefWidth(75.0);
        btn_Pagar.setText("Pagar");
        HBox.setMargin(btn_Pagar, new Insets(0.0, 0.0, 0.0, 5.0));

        asigBtn();
        poblar();
    }
    
    public displayPagos(String nom, String ape, String fecha, String val,
            String est, String des, Anschluss cdb) {
        
        this.cdb = cdb;
        
        lbl_Nombre = new Label();
        lbl_Apellido = new Label();
        lbl_Fecha = new Label();
        lbl_Valor = new Label();
        lbl_Estado = new Label();
        lbl_Descripcion = new Label();
        btn_Pagar = new Button();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefWidth(1150.0);
        setPadding(new Insets(5.0, 0.0, 5.0, 0.0));

        lbl_Nombre.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Nombre.setId("lbl_Nombre");
        lbl_Nombre.setPrefWidth(200.0);
        lbl_Nombre.setText(nom);
        lbl_Nombre.setFont(new Font("Segoe UI Semilight", 16.0));
        HBox.setMargin(lbl_Nombre, new Insets(0.0, 0.0, 0.0, 5.0));

        lbl_Apellido.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Apellido.setId("lbl_Apellido");
        lbl_Apellido.setPrefWidth(200.0);
        lbl_Apellido.setText(ape);
        lbl_Apellido.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Fecha.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Fecha.setId("lbl_Fecha");
        lbl_Fecha.setLayoutX(215.0);
        lbl_Fecha.setLayoutY(15.0);
        lbl_Fecha.setPrefWidth(100.0);
        lbl_Fecha.setText(fecha);
        lbl_Fecha.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Valor.setAlignment(javafx.geometry.Pos.CENTER_RIGHT);
        lbl_Valor.setId("lbl_Valor");
        lbl_Valor.setPrefWidth(75.0);
        lbl_Valor.setText("$" + val.substring(0, val.length() - 2) + "." 
                + val.substring(val.length() - 2));
        lbl_Valor.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Estado.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Estado.setId("lbl_Estado");
        lbl_Estado.setPrefWidth(125.0);
        lbl_Estado.setText(est);
        lbl_Estado.setFont(new Font("Segoe UI Semilight", 16.0));

        lbl_Descripcion.setAlignment(javafx.geometry.Pos.CENTER);
        lbl_Descripcion.setId("lbl_Descripcion");
        lbl_Descripcion.setPrefWidth(350.0);
        lbl_Descripcion.setText(des);
        lbl_Descripcion.setFont(new Font("Segoe UI Semilight", 16.0));

        btn_Pagar.setId("btn_Pagar");
        btn_Pagar.setMnemonicParsing(false);
        btn_Pagar.setPrefWidth(75.0);
        btn_Pagar.setText("Pagar");
        HBox.setMargin(btn_Pagar, new Insets(0.0, 0.0, 0.0, 5.0));
        
        asigBtn();
        poblarTodo();
    }
    
    private void poblarTodo(){
        getChildren().add(lbl_Nombre);
        getChildren().add(lbl_Apellido);
        getChildren().add(lbl_Fecha);
        getChildren().add(lbl_Valor);
        getChildren().add(lbl_Estado);
        getChildren().add(lbl_Descripcion);
        getChildren().add(btn_Pagar);
    }
    
    private void poblar(){
        getChildren().add(lbl_Nombre);
        getChildren().add(lbl_Apellido);
        getChildren().add(lbl_Fecha);
        getChildren().add(lbl_Valor);
        getChildren().add(lbl_Estado);
        getChildren().add(lbl_Descripcion);
    }
    
    private void asigBtn(){
        btn_Pagar.setOnAction((ActionEvent event) -> {
            this.lbl_Estado.setText("PAGADO");
            pagar();
        });
    }
    
    private void pagar(){
        cdb.marcarPagado(lbl_Nombre.getText(), lbl_Apellido.getText(),
                lbl_Fecha.getText(), 0);
    }
}
