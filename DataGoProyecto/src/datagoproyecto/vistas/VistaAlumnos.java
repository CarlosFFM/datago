package datagoproyecto.vistas;

import datagoproyecto.conexion.Anschluss;
import java.util.ArrayList;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public class VistaAlumnos extends AnchorPane {
    
    private Anschluss cdb;

    protected final Label label;
    protected final Label label0;
    protected final HBox hBox;
    protected final VBox vBox;
    protected final VBox vBox2;
    protected final VBox vBox3;
    protected final VBox vBox4;
    protected final Label label1;
    protected final TextField tf_Nombre;
    protected final Label label2;
    protected final TextField tf_Ape;
    protected final Label label3;
    protected final Label label4;
    protected final TextField tf_Ced;
    protected final Button button0;
    protected final ScrollPane scrollPane;
    protected final GridPane gridPane;
    protected final ColumnConstraints columnConstraints;
    protected final ColumnConstraints columnConstraints0;
    protected final RowConstraints rowConstraints;
    protected final RowConstraints rowConstraints0;
    protected final RowConstraints rowConstraints1;
    private ArrayList<displayAlumnos> alumnos;
    

    public VistaAlumnos(Anschluss cdb) {
        
        this.cdb = cdb;

        label = new Label();
        label0 = new Label();
        hBox = new HBox();
        vBox = new VBox();
        label1 = new Label();
        tf_Nombre = new TextField();
        label2 = new Label();
        tf_Ape = new TextField();
        label3 = new Label();
        label4 = new Label();
        vBox2 = new VBox();
        vBox3 = new VBox();
        vBox4 = new VBox();
        tf_Ced = new TextField();
        button0 = new Button();
        scrollPane = new ScrollPane();
        gridPane = new GridPane();
        columnConstraints = new ColumnConstraints();
        columnConstraints0 = new ColumnConstraints();
        rowConstraints = new RowConstraints();
        rowConstraints0 = new RowConstraints();
        rowConstraints1 = new RowConstraints();

        setId("AnchorPane");
        setPrefHeight(720.0);
        setPrefWidth(1150.0);

        label.setId("lb_Warn");
        label.setLayoutX(14.0);
        label.setLayoutY(5.0);
        label.setText("Advertencia!");
        label.setTextFill(javafx.scene.paint.Color.RED);
        label.setFont(new Font("Segoe UI Semilight", 18.0));

        AnchorPane.setLeftAnchor(label0, 0.0);
        AnchorPane.setRightAnchor(label0, 0.0);
        label0.setAlignment(javafx.geometry.Pos.CENTER);
        label0.setLayoutY(32.0);
        label0.setText("Alumnos");
        label0.setFont(new Font(24.0));

        AnchorPane.setLeftAnchor(hBox, 14.0);
        AnchorPane.setRightAnchor(hBox, -14.0);
        hBox.setId("HBox");
        hBox.setLayoutX(14.0);
        hBox.setLayoutY(92.0);
        hBox.setPrefHeight(42.0);
        hBox.setPrefWidth(200.0);

        label1.setId("lb_Nombres");
        label1.setText("Nombres: ");
        label1.setFont(new Font("Segoe UI Semilight", 17.0));

        tf_Nombre.setId("Text_Nombres");
        tf_Nombre.setPromptText("Nombres");

        label2.setId("lb_Apellidos");
        label2.setText("Apellidos: ");
        label2.setFont(new Font("Segoe UI Semilight", 17.0));

        tf_Ape.setId("Text_Apellidos");
        tf_Ape.setPromptText("Apellidos");

        label3.setId("lb_space");
        label3.setText("                                 ");

        label4.setId("lb_Cedula");
        label4.setText("Cedula: ");
        label4.setFont(new Font("Segoe UI Semilight", 17.0));

        tf_Ced.setId("Text_Cedula");
        tf_Ced.setPromptText("Cedula");

        button0.setId("bt_Cedula");
        button0.setMnemonicParsing(false);
        button0.setText("Buscar");
        

        AnchorPane.setBottomAnchor(scrollPane, 0.0);
        AnchorPane.setLeftAnchor(scrollPane, 0.0);
        AnchorPane.setRightAnchor(scrollPane, 0.0);
        scrollPane.setLayoutY(132.0);
        scrollPane.setPrefHeight(578.0);
        scrollPane.setPrefWidth(1150.0);

        columnConstraints.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints.setMinWidth(10.0);
        columnConstraints.setPrefWidth(100.0);

        columnConstraints0.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(100.0);

        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);
        rowConstraints.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints0.setMinHeight(10.0);
        rowConstraints0.setPrefHeight(30.0);
        rowConstraints0.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints1.setMinHeight(10.0);
        rowConstraints1.setPrefHeight(30.0);
        rowConstraints1.setVgrow(javafx.scene.layout.Priority.SOMETIMES);
        scrollPane.setContent(vBox);

        getChildren().add(label);
        getChildren().add(label0);
        hBox.getChildren().add(label1);
        hBox.getChildren().add(tf_Nombre);
        hBox.getChildren().add(label2);
        hBox.getChildren().add(tf_Ape);
        hBox.getChildren().add(label3);
        hBox.getChildren().add(label4);
        hBox.getChildren().add(tf_Ced);
        hBox.getChildren().add(button0);
        getChildren().add(hBox);
        gridPane.getColumnConstraints().add(columnConstraints);
        gridPane.getColumnConstraints().add(columnConstraints0);
        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints0);
        gridPane.getRowConstraints().add(rowConstraints1);
        getChildren().add(scrollPane);
        
        asigBuscar();
        vBox2.getChildren().addAll(new displayAlumnos());
        
        vBox4.getChildren().addAll(vBox2,vBox3);
        
        
        scrollPane.setContent(vBox4);
    }
    
    public void asigBuscar(){           
        button0.setOnAction((event)->{
            vBox3.getChildren().clear();
            String nom = getValorString(tf_Nombre.getText());
            String ape = getValorString(tf_Ape.getText());
            String ced = getValorString(tf_Ced.getText());
            
            alumnos = cdb.buscarAlumno(nom, ape, ced);
            
            for (displayAlumnos dp : alumnos){
                vBox3.getChildren().add(dp);
            }
        });
        
    }
    
    private String getValorString(String s)
    {
        if (s.equals("")){
            return "%";
        } return s;
    }    
}
