CREATE DATABASE CEIMdC;

CREATE USER 'admin'@'%' IDENTIFIED BY 'admin';
GRANT ALL PRIVILEGES ON CEIMdC.* TO 'admin'@'%' WITH GRANT OPTION;

CREATE USER 'Raul'@'%' IDENTIFIED BY 'Emilia';
GRANT SELECT, UPDATE, INSERT, DELETE, EXECUTE ON CEIMdC.* TO 'Raul'@'%';

CREATE USER 'Karla'@'%' IDENTIFIED BY 'Lucia';
GRANT SELECT, UPDATE, INSERT, DELETE, EXECUTE ON CEIMdC.* TO 'Karla'@'%';

CREATE USER 'DepPsicologia'@'%' IDENTIFIED BY 'Nietzsche';
GRANT SELECT, EXECUTE ON CEIMdC.* TO 'DepPsicologia'@'%';

FLUSH PRIVILEGES;

USE CEIMdC;

CREATE TABLE CargoSueldo(
	id INT AUTO_INCREMENT PRIMARY KEY,
	Cargo VARCHAR(10) NOT NULL UNIQUE,
	Sueldo INT NOT NULL); #Valor en centavos
    
CREATE TABLE Horario(
	id INT AUTO_INCREMENT PRIMARY KEY,
	tipo VARCHAR(30),
	horaEnt TIME NOT NULL,
	horaSal TIME NOT NULL);
    
CREATE TABLE Empleado(
	Cedula VARCHAR(10) PRIMARY KEY,
	Nombre VARCHAR(30) NOT NULL,
	Apellido VARCHAR(30) NOT NULL,
	Cargo INT,
	Horario INT,
	FOREIGN KEY (Horario) REFERENCES Horario(id),
	FOREIGN KEY (Cargo) REFERENCES CargoSueldo(id));
    
CREATE TABLE Multa(
	id INT AUTO_INCREMENT PRIMARY KEY,
	valor INT DEFAULT 0, #valor en centavos
	Descripcion VARCHAR(30),
	Fecha DATE NOT NULL,
	Hora TIME NOT NULL,
	Empleado VARCHAR(10),
	FOREIGN KEY (Empleado) REFERENCES Empleado(Cedula));
    
CREATE TABLE Profesor(
	Cedula VARCHAR(10),
	NivelAcademico VARCHAR(10),
    Capacitaciones VARCHAR(30),
	FOREIGN KEY (Cedula) REFERENCES Empleado(Cedula),
	PRIMARY KEY(Cedula));
    
#Los datos de esta tabla provienen de un lector de huellas que
#brinda un reporte periodico en la forma de un csv
CREATE TABLE RegistroEntSal(
	Empleado VARCHAR(10),
	CodEm INT NOT NULL,
	Hora TIME NOT NULL,
	Fecha DATE NOT NULL,
	FOREIGN KEY (Empleado) REFERENCES Empleado(Cedula),
	PRIMARY KEY(Empleado, Hora, Fecha));
    
CREATE TABLE Materia(
	Cod INT AUTO_INCREMENT PRIMARY KEY,
	Nombre VARCHAR(15) NOT NULL);
    
CREATE TABLE Representante(
	Cedula VARCHAR(10) PRIMARY KEY,
	Nombre VARCHAR(30) NOT NULL,
	Apellido VARCHAR(30) NOT NULL,
    FechaNacimiento DATE NOT NULL,
    Nacionalidad VARCHAR(10) NOT NULL,
    Celular VARCHAR(10) NOT NULL,
    NivelEdu VARCHAR(10),
    AutorizadoRetirar BOOLEAN NOT NULL,
    EstadoCivil VARCHAR(10) NOT NULL,
    VCE BOOLEAN NOT NULL,
	Correo VARCHAR(40) NOT NULL,
	Ocupacion VARCHAR(40) NOT NULL);
    
CREATE TABLE Estudiante(
	Cedula VARCHAR(10) PRIMARY KEY,
	Nombre VARCHAR(30) NOT NULL,
	Apellido VARCHAR(30) NOT NULL,
	FechaNacimiento DATE NOT NULL,
    TelefonoCasa VARCHAR(9),
    Nacionalidad VARCHAR(15),
    ContactoEmer VARCHAR(30),
    Direccion VARCHAR(30),
    Representante VARCHAR(10),
    Madre VARCHAR(10),
    Padre VARCHAR(10),
    Activo boolean NOT NULL DEFAULT true,
    FOREIGN KEY (Representante) REFERENCES Representante(Cedula),
    FOREIGN KEY (Madre) REFERENCES Representante(Cedula),
    FOREIGN KEY (Padre) REFERENCES Representante(Cedula));

CREATE TABLE Salon(
	ID INT PRIMARY KEY,
    Nivel VARCHAR(15) NOT NULL,
    Anio INT NOT NULL);

CREATE TABLE Imparte(
	Cod VARCHAR(8) PRIMARY KEY,
	Profesor VARCHAR(10),
	Materia INT,
    Salon INT NOT NULL,
	FOREIGN KEY (Profesor) REFERENCES Profesor(Cedula),
    FOREIGN KEY (Salon) REFERENCES Salon(ID),
    FOREIGN KEY (Materia) REFERENCES Materia(Cod));
    
CREATE TABLE SalonEstudiante(
	Salon INT,
    Estudiante VARCHAR(10),
    FOREIGN KEY (Salon) REFERENCES Salon(ID),
    FOREIGN KEY (Estudiante) REFERENCES Estudiante(Cedula),
    PRIMARY KEY (Salon, Estudiante));
   
CREATE TABLE Representados(
	Representante VARCHAR(10),
	Estudiante VARCHAR(10),
	Relacion VARCHAR(10) NOT NULL,
	FOREIGN KEY (Estudiante) REFERENCES Estudiante(Cedula),
	FOREIGN KEY (Representante) REFERENCES Representante(Cedula),
	PRIMARY KEY(Representante, Estudiante));
    
CREATE TABLE Pagos(
	Estudiante VARCHAR(10),
	MesAnio VARCHAR(7) NOT NULL,
	Estado VARCHAR(9) DEFAULT "SIN PAGAR",
	Descripcion VARCHAR(300),
	Valor INT, #Valor en centavos
    Atrasado BOOLEAN NOT NULL DEFAULT false,
	FOREIGN KEY (Estudiante) REFERENCES Estudiante(Cedula),
	PRIMARY KEY(Estudiante, MesAnio));
    
DELIMITER $

CREATE PROCEDURE getPagos(IN ced VARCHAR(10), IN fecha VARCHAR(7))
	BEGIN
		SELECT Nombre, Apellido, MesAnio, Valor, Estado, Descripcion
        FROM (SELECT Cedula, Nombre, Apellido FROM Estudiante WHERE Cedula LIKE ced) e
        JOIN
        (SELECT Estudiante, MesAnio, Valor, Estado, Descripcion FROM Pagos WHERE MesAnio LIKE fecha) p
        ON p.Estudiante = e.Cedula;
	END$
    
DELIMITER ;

DELIMITER **

CREATE TRIGGER crearPagos AFTER INSERT ON Estudiante FOR EACH ROW
	BEGIN
		INSERT INTO Pagos(Estudiante, MesAnio, Descripcion, Valor)
        VALUES (NEW.cedula, '04-2019', 'Pension', 14000),
        (NEW.cedula, '05-2019', 'Pension', 14000),
        (NEW.cedula, '06-2019', 'Pension', 14000),
        (NEW.cedula, '07-2019', 'Pension', 14000),
        (NEW.cedula, '08-2019', 'Pension', 14000),
        (NEW.cedula, '09-2019', 'Pension', 14000),
        (NEW.cedula, '10-2019', 'Pension', 14000),
        (NEW.cedula, '11-2019', 'Pension', 14000),
        (NEW.cedula, '12-2019', 'Pension', 14000),
        (NEW.cedula, '01-2020', 'Pension', 14000);
	END**

DELIMITER ;

DELIMITER !!

CREATE PROCEDURE marcarPagado(IN nom VARCHAR(30), IN ape VARCHAR(30), IN mesAn VARCHAR(7))
	BEGIN
		UPDATE Pagos SET Estado = 'PAGADO', Atrasado = 0
        WHERE Pagos.Estudiante IN (
			SELECT Cedula FROM Estudiante WHERE Nombre = nom AND Apellido = ape)
		AND MesAnio = mesAn;
	END!!

DELIMITER ;

DELIMITER $$

CREATE PROCEDURE consultaMulta(in nom VARCHAR(30), in ap VARCHAR(30), in fec VARCHAR(10))
	BEGIN
		SELECT e.nombre, e.apellido,m.valor, m.descripcion ,m.fecha,m.hora, cs.cargo
        FROM empleado e 
        JOIN multa m on e.Cedula=m.Empleado 
        JOIN cargosueldo cs on cs.id = e.Cargo
        WHERE e.Nombre LIKE nom AND e.Apellido LIKE ap AND m.Fecha LIKE fec;
	END $$pma__userconfig

DELIMITER ;

DELIMITER $$
CREATE PROCEDURE consultaAlumno(in nom VARCHAR(30), in ap VARCHAR(30), in ce VARCHAR(10))
	begin 
		SELECT Cedula, Nombre, Apellido, FechaNacimiento, TelefonoCasa, Nacionalidad, ContactoEmer, Direccion, Representante, Madre, Padre, Activo
        FROM Estudiante
        WHERE Nombre LIKE nom AND Apellido LIKE ap AND Cedula LIKE ce;
	END $$
DELIMITER ;inpagos

#DROP VIEW Profs;

#CREATE VIEW Profs AS (SELECT Nombre, Apellido, p.Capacitaciones, i.materia, s.nivel, COUNT(m.id) AS Multas FROM (SELECT * FROM Empleado WHERE Cedula = '0706412773') e JOIN Profesor p on e.Cedula = p.Cedula JOIN Imparte i on p.Cedula = i.Profesor JOIN Salon s ON i.Salon = s.ID JOIN Multa m on m.Empleado = e.Cedula);

#SELECT * FROM Profs;